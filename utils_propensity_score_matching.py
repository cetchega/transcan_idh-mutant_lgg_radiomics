#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 22:23:04 2023

@author: cetchega
"""
##### IMPORTS #####

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from lifelines.statistics import logrank_test
from lifelines import KaplanMeierFitter
import matplotlib.patches as mpatches
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import NearestNeighbors
import matplotlib 
import os
matplotlib.rcParams['axes.facecolor'] = 'white'
matplotlib.rcParams['axes.edgecolor'] = 'black'

##### FUNCTIONS #####

def read_data(imtype,covar,target):
    data = pd.read_excel("databases/Clinical_database.xlsx",sheet_name=0)
    data["EOR"] = data["EOR"].apply(lambda x: 0*(x=='Total')+1*(x=='Subtotal')+2*(x=='Partial')+3*(x=='Biopsy'))
    data['ADJUVANT Rx'] = data['ADJUVANT Rx'].astype(int)
    data['Sex'] = data['Sex'].astype(int)
    data['Grade'] = data['Grade'].astype(int)
    data["PET"] = data["PET"].apply(lambda x: 0*(x=='neg')+1*(x=='pos'))
    data["Histology"] = data["Histology"].apply(lambda x: 0*(x=='Astrocytoma')+1*(x=='Oligodendroglioma'))
    data = data[['PatID']+covar].merge(target,how='inner',on='PatID') #'Survival_group',,'Recurrence_free_survival','RECURRENCE'
    return data

def get_logrank_pv(db):
    T = db['PFS'].apply(lambda time: time/12) #duration
    C = db["RECURRENCE"] #censorship : 1 if recurrence, 0 if censored
    targ = db["Survival_group"]
    # Logrank test
    results = logrank_test(T.loc[(targ == 0)],T.loc[(targ == 1)],C.loc[(targ == 0)],C.loc[(targ == 1)])
    return results.p_value

def get_KM(db,signature,title,saving_dir):
    matplotlib.rcParams['axes.facecolor'] = 'white'
    matplotlib.rcParams['axes.edgecolor'] = 'black'
    
    T = db['PFS'].apply(lambda time: time/12) #duration
    C = db["RECURRENCE"] #censorship : 1 if recurrence, 0 if censored
    targ = db["Survival_group"]
    
    # Logrank test
    results = logrank_test(T.loc[(targ == 0)],T.loc[(targ == 1)],C.loc[(targ == 0)],C.loc[(targ == 1)])
    pv = results.p_value
    
    kmf = KaplanMeierFitter()
    palette = ["windows blue","tomato"]
    plt_palette = ["xkcd:"+name for name in palette]
    sns.set_palette(sns.xkcd_palette(palette))
    
    ##SET UP PLOT
    f = plt.figure(figsize=(10,6))
    ax = f.add_subplot(111)
    if pv < 0.001:
        ax.text(0.5,.05,'$p<0.001$',fontsize=18)
    elif pv<0.050 and round(pv,3)==0.050:
        ax.text(0.5,.05,'$p=%0.4f$' % pv,fontsize=18)
    else:
        ax.text(0.5,.05,'$p=%0.3f$' % pv,fontsize=18)
    plt.title(r''+signature ) 
    plt.xlabel('Time (years)')
    plt.ylabel('Recurrence-free probability')
    sns.set_context("talk")
    #plt.title('Kaplan-Meier curve')
    d={} #to store the models
    i=0
    max_surv=0

    #loop through segmentation variable, plot on same axes
    for segment in sorted(targ.unique()): 
        ix = (targ == segment)
        d['kmf{}'.format(i+1)] = kmf.fit(T.loc[ix],C.loc[ix], label=segment)
        ax = kmf.plot(ax=ax, show_censors=True, ci_alpha=0.1, censor_styles={'ms': 6, 'marker': 's'})
        ax.set_xlim([T.min(),T.max()])
        ax.set_ylim([0,1])
        
        # Median survival 
        med_surv_time = np.amin([kmf.median_survival_time_,np.amax(T.values)])
        plt.vlines(med_surv_time,0,0.5,[plt_palette[i]],'dashed')
        max_surv = np.amax([max_surv,med_surv_time])
        i+=1

    ##LEGEND
    plt.hlines(0.5,0,max_surv,["grey"],'dashed')
    patches = [ 
                mpatches.Patch(color="xkcd:windows blue", label= r' Smaller score'),
                mpatches.Patch(color="xkcd:tomato", label= r' Larger score')
              ]

    plt.legend(handles=[patches[0],patches[1]],bbox_to_anchor=(0, 1, 0, -1.2),loc='upper left')
    plt.xlabel('Time (years)')
    
    #ax.set_facecolor("none")
    plt.tight_layout()
    f.savefig(os.path.join(saving_dir,title+'_Propensity_KM.pdf'))
    return None

def PropScoreMatching(survival_df,target,imtype,covar,kn,saving_dir):
    ## Get data
    if survival_df.shape[0] != target.shape[0]:
        print("Wrong number of patients")
        return None
    # Get covariates df 
    covar_data = read_data(imtype,covar,target)
    
    stat_columns = ['signature','nb_total_EOR','pct_total_EOR','nb_adjuvant_1','pct_adjuvant_1','age_mean','age_std','nb_oligo','pct_oligo','nb_grade_2','pct_grade_2','nb_grade_3','pct_grade_3','nb_grade_4','pct_grade_4','nb_sex_1','pct_sex_1','nb_PET_pos','pct_PET_pos']
    stat_df = pd.DataFrame(columns=stat_columns)
    
    logrank_pv_df = pd.DataFrame(columns=['Signatures','LogRank_pv','LogRank_pv_after_psm','Nb_of_patients'])
    
    # Loop on the signatures
    signature_list = [name for name in survival_df.columns.values if name != 'PatID']
    for signature in signature_list:
        data = covar_data.merge( survival_df[['PatID',signature]], how='inner', on='PatID' ) 
        data.loc[:,'Survival_group'] = data.loc[:,signature]
        
        # choose features for propensity score calculation
        X = data[covar]
        y = data.Survival_group.values
        
        # use logistic regression to calculate the propensity scores
        lr = LogisticRegression(solver='saga',max_iter=100000)
        try:
            lr.fit(X, y)
        except:
            print(signature)
        else:
            # prediction of survival group from covars
            pred_prob = lr.predict_proba(X)  # probabilities for classes
            # the propensity score (ps) is the probability of being 1 
            data['ps'] = pred_prob[:, 1]
            #print("Identification of neighbours...")
            # use 20% of standard deviation of the propensity score as the caliper/radius
            # get the k closest neighbors for each observations
            # relax caliper and increase k can provide more matches
            caliper = np.std(data.ps) * 0.2 
            n_neighbors = kn
            # setup knn
            knn = NearestNeighbors(n_neighbors=n_neighbors, radius=caliper)
            ps = data[['ps']]  
            knn.fit(ps)
            
            # distances and indexes
            distances, neighbor_indexes = knn.kneighbors(ps)

            # for each point in 1, we find a matching point in 0 without replacement
            # note the n neighbors may include both points in 1 and 0
            matched_control = []  # keep track of the matched observations in 0
            
            for current_index, row in data.iterrows():  # iterate over the dataframe
                if row['Survival_group'] == 0:  # the current row is in the control group
                    data.loc[current_index, 'matched'] = np.nan  # set matched to nan
                else: 
                    for idx in neighbor_indexes[current_index, :]: # for each row in 1, find the k neighbors
                        # make sure the current row is not the idx - don't match to itself
                        # and the neighbor is in 0 
                        if (current_index != idx) and (data.loc[idx,signature] == 0):
                            if idx not in matched_control:  # this control has not been matched yet
                                #print(idx,matched_control)
                                data.loc[current_index, 'matched'] = idx  # record the matching
                                matched_control.append(idx)  # add the matched to the list
                                break
            # control have no match
            treatment_matched = data.dropna(subset=['matched'])  # drop not matched
        
            # matched control observation indexes
            control_matched_idx = treatment_matched.matched
            control_matched_idx = control_matched_idx.astype(int)  # change to int
            control_matched = data.loc[control_matched_idx, :]  # select matched control observations
        
            # combine the matched treatment and control
            df_matched = pd.concat([treatment_matched, control_matched])
            # matched control and treatment
            df_matched_control = df_matched[df_matched[signature]==0]
            df_matched_treatment = df_matched[df_matched[signature]==1]
            
            # Comparison of data and df_matched
            ## High signature
            n_tot = df_matched_treatment.shape[0]
            n_EOR_tot = df_matched_treatment['EOR'][df_matched_treatment['EOR'] == 0].shape[0]
            n_adj = df_matched_treatment['ADJUVANT Rx'][df_matched_treatment['ADJUVANT Rx'] == 1].shape[0]
            try:
                n_petp = df_matched_treatment['PET'][df_matched_treatment['PET'] == 1].shape[0]
            except:
                 # Ouvrir avec covar_mri et récupérer les infos. 
                data_with_pet = read_data('MRI',['EOR','Age','ADJUVANT Rx','Sex','PET','Histology','Grade'],target)
                aux = data_with_pet[['PatID','PET']][data_with_pet['PatID'].isin(df_matched_treatment['PatID'])]
                n_petp = aux['PET'][aux['PET'] == 1].shape[0]
                pass
            n_oligo = df_matched_treatment['Histology'][df_matched_treatment['Histology'] == 1].shape[0]
            n_g2 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 2].shape[0]
            n_g3 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 3].shape[0]    
            n_g4 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 4].shape[0] 
            n_sx = df_matched_treatment['Sex'][df_matched_treatment['Sex'] == 1].shape[0] 
            # Store Nb of pats in each category for high score
            stats_list = [signature+'_high',n_EOR_tot,n_EOR_tot *100 / n_tot,n_adj,n_adj *100 / n_tot, df_matched_treatment['Age'].mean(),df_matched_treatment['Age'].std()]
            stats_list += [n_oligo,n_oligo *100/n_tot,n_g2,n_g2 *100/n_tot,n_g3,n_g3 *100/n_tot,n_g4,n_g4 *100/n_tot,n_sx,n_sx *100/n_tot]  
            stats_list += [n_petp,n_petp*100/n_tot]
            stat_df.loc[len(stat_df)] = stats_list
            
            ## Low Signature
            n_tot = df_matched_control.shape[0]
            n_EOR_tot = df_matched_control['EOR'][df_matched_control['EOR'] == 0].shape[0]
            n_adj = df_matched_control['ADJUVANT Rx'][df_matched_control['ADJUVANT Rx'] == 1].shape[0]
            try:
                n_petp = df_matched_control['PET'][df_matched_control['PET'] == 1].shape[0]
            except:
                 # Ouvrir avec covar_mri et récupérer les infos. 
                data_with_pet = read_data('MRI',['EOR','Age','ADJUVANT Rx','Sex','PET','Histology','Grade'],target)
                aux = data_with_pet[['PatID','PET']][data_with_pet['PatID'].isin(df_matched_control['PatID'])]
                n_petp = aux['PET'][aux['PET'] == 1].shape[0]
                pass
            n_oligo = df_matched_control['Histology'][df_matched_control['Histology'] == 1].shape[0]
            n_g2 = df_matched_control['Grade'][df_matched_control['Grade'] == 2].shape[0]
            n_g3 = df_matched_control['Grade'][df_matched_control['Grade'] == 3].shape[0]    
            n_g4 = df_matched_control['Grade'][df_matched_control['Grade'] == 4].shape[0] 
            n_sx = df_matched_control['Sex'][df_matched_control['Sex'] == 1].shape[0] 
            
            # Store Nb of pats in each category for low score
            stats_list = [signature+'_low',n_EOR_tot,n_EOR_tot *100 / n_tot,n_adj,n_adj *100 / n_tot, df_matched_treatment['Age'].mean(),df_matched_treatment['Age'].std()]
            stats_list += [n_oligo,n_oligo *100/n_tot,n_g2,n_g2 *100/n_tot,n_g3,n_g3 *100/n_tot,n_g4,n_g4 *100/n_tot,n_sx,n_sx *100/n_tot]
            stats_list += [n_petp,n_petp*100/n_tot]
            stat_df.loc[len(stat_df)] = stats_list
            
            ## KM 
            pv_after = get_logrank_pv(df_matched)
            pv = get_logrank_pv(data)
            
            get_KM(df_matched,signature,imtype+'_'+signature+"_after_matching",saving_dir)
            get_KM(data,signature,imtype+'_'+signature+"_before_matching",saving_dir)
            
            # check the overlap of ps for 0 and 1 using histogram
            # if not much overlap, the matching won't work
            plt.figure()
            sns.histplot(data=data, x='ps', hue='Survival_group')  
            plt.xlabel('Propensity Scores')
            plt.savefig(saving_dir+'/'+imtype+'_'+signature+'_Propensity_Scores.pdf')
            
            df_before = data.groupby(signature).mean()
            df_after = df_matched.groupby(signature).mean()
            df_before.to_csv(os.path.join(saving_dir,imtype+'_'+signature+"_before_matching.csv"))
            df_after.to_csv(os.path.join(saving_dir,imtype+'_'+signature+"_after_matching.csv"))
                
            logrank_pv_df.loc[len(logrank_pv_df)] = [signature.replace('$',''),round(pv,3),round(pv_after,3),df_matched.shape[0]] 
            
    stat_df.to_csv(os.path.join(saving_dir,'covariates_repartition_after_matching.csv'))
    
    logrank_pv_df = logrank_pv_df.sort_values(by='LogRank_pv_after_psm', ascending=True)
    logrank_pv_df.to_csv(os.path.join(saving_dir,'logrank_results.csv'))
    plt.close('all')
    return None