#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 16:01:03 2023

Benchmarking of the radiomics signature of https://pubmed.ncbi.nlm.nih.gov/30696801/
"IDH mutation-specific radiomic signature in lower-grade gliomas", 2019, Aging. 

Images are T2-weighted MRI
Preprocessing: intensity normalization - no N4 bias correction
Voxel sizes: 0.6*0.6*5 mm^3
IDH-mutant LGG database: 118 patients in training set, 77 patients in test set. 
Identification of radiomic risk score for OS: univariate Cox p-value and hazard ratio
Splitting cutoff optimized on the training set (p-value). Cutoff value not found in the article. 
Kaplan-Meier curve on 109 patients (patients with 0S <= 30 days were removed). 

Our database: 
    150 patients
    Voxel size: 0.5^3 mm^3
    

@author: cetchega
"""

import os
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns
from lifelines.statistics import logrank_test
from lifelines import KaplanMeierFitter
import matplotlib.patches as mpatches
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import NearestNeighbors


##### FUNCTIONS 

def get_survival_db(pat,covar):
    # read in your data
    data = pd.read_excel(os.path.join("databases","Clinical_database.xlsx"),sheet_name=0)
    data["EOR"] = data["EOR"].apply(lambda x: 0*(x=='Total')+1*(x=='Subtotal')+2*(x=='Partial')+3*(x=='Biopsy'))
    data['ADJUVANT Rx'] = data['ADJUVANT Rx'].astype(int)
    data['Sex'] = data['Sex'].astype(int)
    data['RECURRENCE'] = data['RECURRENCE'].astype(int)
    data['Grade'] = data['Grade'].astype(int)
    data["PET"] = data["PET"].apply(lambda x: 0*(x=='neg')+1*(x=='pos'))
    data["Histology"] = data["Histology"].apply(lambda x: 0*(x=='Astrocytoma')+1*(x=='Oligodendroglioma'))

    data = data[['PatID','Recurrence_free_survival','OS','RECURRENCE']+covar]
    data = data[data['PatID'].isin(pat)].reset_index(drop=True)

    print("Shape of data : ",data.shape)
    return data

def get_KM(db,title,figname):
    T = db['OS'] #duration
    C = db["RECURRENCE"] #censorship : 1 if recurrence, 0 if censored
    targ = db["Survival_group"]
    
    # Logrank test
    results = logrank_test(T.loc[(targ == 0)],T.loc[(targ == 1)],C.loc[(targ == 0)],C.loc[(targ == 1)])
    pv = results.p_value
    statistics = results.test_statistic
    
    kmf = KaplanMeierFitter()
    palette = ["windows blue","tomato"]
    plt_palette = ["xkcd:"+name for name in palette]
    sns.set_palette(sns.xkcd_palette(palette))
    
    ##SET UP PLOT
    f = plt.figure(figsize=(10,6))
    ax = f.add_subplot(111)
    ax.text(0.5,.05,'$p=%0.3f$' % pv,fontsize=18)
    plt.xlabel('Time (years)')
    plt.ylabel('Recurrence probability')
    sns.set_context("talk")
    plt.title(r''+figname)
    d={} #to store the models
    i=0
    max_surv=0

    #loop through segmentation variable, plot on same axes
    for segment in sorted(targ.unique()): 
        ix = (targ == segment)
        d['kmf{}'.format(i+1)] = kmf.fit(T.loc[ix],C.loc[ix], label=segment)
        ax = kmf.plot(ax=ax, show_censors=True, ci_alpha=0.1, censor_styles={'ms': 6, 'marker': 's'})
        ax.set_xlim([T.min(),T.max()])
        ax.set_ylim([0,1])
        
        # Median survival 
        med_surv_time = np.amin([kmf.median_survival_time_,np.amax(T.values)])
        plt.vlines(med_surv_time,0,0.5,[plt_palette[i]],'dashed')
        max_surv = np.amax([max_surv,med_surv_time])
        i+=1

    ##LEGEND
    plt.hlines(0.5,0,max_surv,["grey"],'dashed')
    patches = [ 
                mpatches.Patch(color="xkcd:windows blue", label= r' Smaller score'),
                mpatches.Patch(color="xkcd:tomato", label= r' Larger score')
              ]

    plt.legend(handles=[patches[0],patches[1]],bbox_to_anchor=(0, 1, 0, -1.2),loc='upper left');#],patches[2] title="Groups",
    plt.xlabel('Time (years)')
    plt.tight_layout()
    f.savefig(os.path.join(saving_dir,title+'_KM.pdf'))
    return pv,statistics

def PropScoreMatching(data,imtype_title,covar,kn,figname): 
    stat_columns = ['signature','nb_total_EOR','pct_total_EOR','nb_adjuvant_1','pct_adjuvant_1','age_mean','age_std','nb_oligo','pct_oligo','nb_grade_2','pct_grade_2','nb_grade_3','pct_grade_3','nb_grade_4','pct_grade_4','nb_sex_1','pct_sex_1','nb_PET_pos','pct_PET_pos']
    stat_df = pd.DataFrame(columns=stat_columns)
    
    logrank_pv_df = pd.DataFrame(columns=['Signatures','LogRank_pv','LogRank_pv_after_psm','Nb_of_patients'])
    
    # choose features for propensity score calculation
    X = data[covar]
    y = data.Survival_group.values
    
    # use logistic regression to calculate the propensity scores
    lr = LogisticRegression(solver='saga',max_iter=100000)
    lr.fit(X, y)
    pred_prob = lr.predict_proba(X)  # probabilities for classes
    # the propensity score (ps) is the probability of being 1 
    data['ps'] = pred_prob[:, 1]
        
    #print("Identification of neighbours...")
    # use 20% of standard deviation of the propensity score as the caliper/radius
    # get the k closest neighbors for each observations
    # relax caliper and increase k can provide more matches
    caliper = np.std(data.ps) * 0.2 
    n_neighbors = kn
    # setup knn
    knn = NearestNeighbors(n_neighbors=n_neighbors, radius=caliper)
    ps = data[['ps']]  
    knn.fit(ps)
            
    # distances and indexes
    distances, neighbor_indexes = knn.kneighbors(ps)

    # for each point in 1, we find a matching point in 0 without replacement
    # note the n neighbors may include both points in 1 and 0
    matched_control = []  # keep track of the matched observations in 0

    for current_index, row in data.iterrows():  # iterate over the dataframe
        if row['Survival_group'] == 0:  # the current row is in the control group
            data.loc[current_index, 'matched'] = np.nan  # set matched to nan
        else: 
            for idx in neighbor_indexes[current_index, :]: # for each row in 1, find the k neighbors
                # make sure the current row is not the idx - don't match to itself
                # and the neighbor is in 0 
                if (current_index != idx) and (data.loc[idx,'Survival_group'] == 0):
                    if idx not in matched_control:  # this control has not been matched yet
                        #print(idx,matched_control)
                        data.loc[current_index, 'matched'] = idx  # record the matching
                        matched_control.append(idx)  # add the matched to the list
                        break

    # control have no match
    treatment_matched = data.dropna(subset=['matched'])  # drop not matched

    # matched control observation indexes
    control_matched_idx = treatment_matched.matched
    control_matched_idx = control_matched_idx.astype(int)  # change to int
    control_matched = data.loc[control_matched_idx, :]  # select matched control observations

    # combine the matched treatment and control
    df_matched = pd.concat([treatment_matched, control_matched])
    print("Check 1-1 matching : ")
    print(df_matched['Survival_group'].value_counts())
    
    # matched control and treatment
    df_matched_control = df_matched[df_matched['Survival_group']==0]
    df_matched_treatment = df_matched[df_matched['Survival_group']==1]
    
    print('High Signature')
    n_tot = df_matched_treatment.shape[0]
    n_EOR_tot = df_matched_treatment['EOR'][df_matched_treatment['EOR'] == 0].shape[0]
    n_adj = df_matched_treatment['ADJUVANT Rx'][df_matched_treatment['ADJUVANT Rx'] == 1].shape[0]
    n_petp = df_matched_treatment['PET'][df_matched_treatment['PET'] == 1].shape[0]
    n_oligo = df_matched_treatment['Histology'][df_matched_treatment['Histology'] == 1].shape[0]
    n_g2 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 2].shape[0]
    n_g3 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 3].shape[0]    
    n_g4 = df_matched_treatment['Grade'][df_matched_treatment['Grade'] == 4].shape[0] 
    n_sx = df_matched_treatment['Sex'][df_matched_treatment['Sex'] == 1].shape[0] 
    
    # Store Nb of pats in each category for high score
    stats_list = ['signature'+'_high',n_EOR_tot,n_EOR_tot *100 / n_tot,n_adj,n_adj *100 / n_tot, df_matched_treatment['Age'].mean(),df_matched_treatment['Age'].std()]
    stats_list += [n_oligo,n_oligo *100/n_tot,n_g2,n_g2 *100/n_tot,n_g3,n_g3 *100/n_tot,n_g4,n_g4 *100/n_tot,n_sx,n_sx *100/n_tot]
    stats_list += [n_petp,n_petp*100/n_tot]

    stat_df.loc[len(stat_df)] = stats_list
    
    print("Nb of total EOR : ",n_EOR_tot)
    print("pct : ",n_EOR_tot *100 / n_tot)
    print("Nb of ADJUVANT Rx = 1 : ",n_adj)
    print("pct : ",n_adj *100 / n_tot)
    print('Age :', df_matched_treatment['Age'].mean(),' +/- ',df_matched_treatment['Age'].std())
    print("Nb of Pet pos : ",n_petp)
    print("pct : ",n_petp *100 / n_tot)
    print("Nb of Oligo : ",n_oligo)
    print("pct : ",n_oligo *100 / n_tot)
    print("Nb of Grade 2 : ",n_g2)
    print("pct : ",n_g2 *100 / n_tot)
    print("Nb of Grade 3 : ",n_g3)
    print("pct : ",n_g3 *100 / n_tot)
    print("Nb of Grade 4 : ",n_g4)
    print("pct : ",n_g4 *100 / n_tot)
    print("Nb of Sex 1 : ",n_sx)
    print("pct : ",n_sx *100 / n_tot)

    print('Low Signature')
    n_tot = df_matched_control.shape[0]
    n_EOR_tot = df_matched_control['EOR'][df_matched_control['EOR'] == 0].shape[0]
    n_adj = df_matched_control['ADJUVANT Rx'][df_matched_control['ADJUVANT Rx'] == 1].shape[0]
    n_petp = df_matched_control['PET'][df_matched_control['PET'] == 1].shape[0]
    n_oligo = df_matched_control['Histology'][df_matched_control['Histology'] == 1].shape[0]
    n_g2 = df_matched_control['Grade'][df_matched_control['Grade'] == 2].shape[0]
    n_g3 = df_matched_control['Grade'][df_matched_control['Grade'] == 3].shape[0]    
    n_g4 = df_matched_control['Grade'][df_matched_control['Grade'] == 4].shape[0] 
    n_sx = df_matched_control['Sex'][df_matched_control['Sex'] == 1].shape[0] 
    
    # Store Nb of pats in each category for low score
    stats_list = ['signature'+'_low',n_EOR_tot,n_EOR_tot *100 / n_tot,n_adj,n_adj *100 / n_tot, df_matched_treatment['Age'].mean(),df_matched_treatment['Age'].std()]
    stats_list += [n_oligo,n_oligo *100/n_tot,n_g2,n_g2 *100/n_tot,n_g3,n_g3 *100/n_tot,n_g4,n_g4 *100/n_tot,n_sx,n_sx *100/n_tot]  
    stats_list += [n_petp,n_petp*100/n_tot]
    
    stat_df.loc[len(stat_df)] = stats_list
    
    print("Nb of total EOR : ",n_EOR_tot)
    print("pct : ",n_EOR_tot *100 / n_tot)
    print("Nb of ADJUVANT Rx = 1 : ",n_adj)
    print("pct : ",n_adj *100 / n_tot)
    print('Age :', df_matched_control['Age'].mean(),' +/- ',df_matched_control['Age'].std())
    print("Nb of Pet pos : ",n_petp)
    print("pct : ",n_petp *100 / n_tot)
    print("Nb of Oligo : ",n_oligo)
    print("pct : ",n_oligo *100 / n_tot)
    print("Nb of Grade 2 : ",n_g2)
    print("pct : ",n_g2 *100 / n_tot)
    print("Nb of Grade 3 : ",n_g3)
    print("pct : ",n_g3 *100 / n_tot)
    print("Nb of Grade 4 : ",n_g4)
    print("pct : ",n_g4 *100 / n_tot)
    print("Nb of Sex 1 : ",n_sx)
    print("pct : ",n_sx *100 / n_tot)

    ## KM 
    pv_after = get_logrank_pv(df_matched)
    pv = get_logrank_pv(data)
    
    if True: #pv_after < 0.10:
        get_KM(df_matched,imtype_title+"_after_matching",figname)
        get_KM(data,imtype_title+"_before_matching",figname)
        
        # check the overlap of ps for 0 and 1 using histogram
        # if not much overlap, the matching won't work
        plt.figure()
        sns.histplot(data=data, x='ps', hue='Survival_group')  
        plt.xlabel('Propensity Scores')
        plt.savefig(os.path.join(saving_dir,imtype_title+'_Propensity_Scores.pdf'))
        
        df_before = data.groupby('Survival_group').mean()
        df_after = df_matched.groupby('Survival_group').mean()
        df_before.to_csv(os.path.join(saving_dir,imtype_title+"_before_matching.csv"))
        df_after.to_csv(os.path.join(saving_dir,imtype_title+"_after_matching.csv"))
        
    logrank_pv_df.loc[len(logrank_pv_df)] = ['signature'.replace('$',''),round(pv,3),round(pv_after,3),df_matched.shape[0]] 
    
    stat_df.to_csv(os.path.join(saving_dir,'covariates_repartition_after_matching.csv'))
    
    logrank_pv_df = logrank_pv_df.sort_values(by='LogRank_pv_after_psm', ascending=True)
    logrank_pv_df.to_csv(os.path.join(saving_dir,'logrank_results.csv'))

    return None

def get_logrank_pv(db):
    T = db['OS']#.apply(lambda time: time/12) #duration
    C = db["RECURRENCE"] #censorship : 1 if recurrence, 0 if censored
    targ = db["Survival_group"]
    
    # Logrank test
    results = logrank_test(T.loc[(targ == 0)],T.loc[(targ == 1)],C.loc[(targ == 0)],C.loc[(targ == 1)])
    return results.p_value



##### PROG #####
## INITIALIZATION

# Folder
saving_dir = os.path.join(os.getcwd(),'Benchmarking')
if os.path.isdir(saving_dir) == False:
    os.mkdir(saving_dir)
    
# Get pyradiomics database
db = pd.read_csv(os.path.join('databases','pyradiomics_output_MRI_wavelets.csv'),index_col=0)
db = db.drop(columns = [item for item in db.columns if item.startswith("diagnostics")]+['Image','Mask'])
pat = list(db.PatID.values)

# Get survival database
covar = ['EOR','Age','ADJUVANT Rx','Sex','PET',"Histology",'Grade']
surv_db = get_survival_db(pat,covar)
target_df = surv_db[['OS','RECURRENCE']]


# Radiomic signature components
# 14 features involved in the signatures, two of them having null weights and therefore removed : 
# Energy_HLL (group 1 derived) and Variance_HLL(group 1 derived)
sig_feats = ["Mean_HLL","Median_HHL","Mean absolute deviation_HLL","RunPercentage_HLL","Range_HLH",
             "Surface to Volume Ratio ","Entropy (group 1) ","Median_HLH","Mean_HHL","Dissimilarity_HLL",
             "Root mean square_HLL","Maximum_HLH"]
sig_betas = [0.027,1.091,0.011,0.263,0.263,5.352,-1.052,7.070,0.173,0.565,0.007,0.001]
pyrad_feats = ["wavelet-HLL_firstorder_Mean",'wavelet-HHL_firstorder_Median','wavelet-HLL_firstorder_MeanAbsoluteDeviation',
               'wavelet-HLL_glrlm_RunPercentage','wavelet-HLH_firstorder_Range','original_shape_SurfaceVolumeRatio',
               'original_firstorder_Entropy','wavelet-HLH_firstorder_Median','wavelet-HHL_firstorder_Mean',
               'wavelet-HLL_glcm_DifferenceAverage','original_firstorder_RootMeanSquared','wavelet-HLH_firstorder_Maximum']


# Compute signature 
feats_db = StandardScaler().fit_transform(db.loc[:,pyrad_feats])
for i in range(len(pyrad_feats)):
    feats_db[:,i] = feats_db[:,i]*sig_betas[i]
sig_db = pd.DataFrame()
sig_db['PatID']=db['PatID']
sig_db['signature'] = feats_db.sum(axis=1)
# Get final db with covariates and outcome
final_df = surv_db.merge(sig_db[['PatID','signature']], how='outer', on='PatID')


# Apply median cutoff and Kaplan-Meier analysis with propensity score matching
med = final_df['signature'].median()
final_df['Survival_group'] = 1*(final_df['signature']>med)
feat_title = 'Median = '+str(round(med,2))
PropScoreMatching(final_df,feat_title,covar,20,'Benchmarking_Median='+str(round(med,2)))









