#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 15:37:13 2023

Code of the Transcan classification analysis.
@author: cetchega

im_type in ['MRI','PET','MRI_PET','genes','genes+MRI','genes+PET','genes+MRI_PET']
outcome_name in ['ColdDiffuse', 'AstroOligo', 'CODA'], CODA for Cold-Oligo Vs Diffuse Astro

Pipeline
- Spearman-correlation based features filtering : drop features having at least one correlation above 0.95 ; keeping features order.
It is performed on the training set outside of the pipeline object.
- Scaler : standard ou robust
- Reducer : univariates (MW, LR), Sparse Group Lasso, SelectKBest with mutual info, SelectFromModel RF.
MW_selector ; LR_selector ; sgl_selector ; SelectKBest(mutual_info_classif) ; SelectFromModel(RandomForestClassifier()) avec option max_features=int(np.sqrt(len(target))) ; [...]
- Classifier : LR (with regularization), RF, SVMs

"""

##### IMPORTS #####
import os
import numpy as np
import pandas as pd
import math

from sklearn.pipeline import Pipeline
from pipelinehelper import PipelineHelper
from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn.feature_selection import SelectFromModel, SelectFpr, RFE
from groupyr import LogisticSGL
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from scipy.stats import spearmanr

from utils_general import prepare_database, adapt_featnames_list
from utils_visualize import PCA_images, PCA_kD
from utils_classification import univariate_mannwhitney_bonferroni, reducer_MannWhitney, reducer_LogisticRegression
from utils_classification import attribute_groups_to_features, get_indices_of_GroupedFeatures
from utils_classification import write_pipeline, nested_cv, cv_plot


##### PROG #####
## 0) INITIALIZATION
outcome_name = 'ColdDiffuse'
im_type = 'MRI'

nb_repetition = 1

## 1) GENERATE CLEAN DATABASE
# Folders 
output_dir = os.path.join('results',outcome_name+'_'+im_type)
if os.path.isdir(output_dir) == False:
    os.mkdir(output_dir)
visudir = os.path.join(output_dir,'visualization')
if os.path.isdir(visudir) == False:
    os.mkdir(visudir)
univdir = os.path.join(output_dir,'univariate')
if os.path.isdir(univdir) == False:
    os.mkdir(univdir)
classifdir = os.path.join(output_dir,'classification')
if os.path.isdir(classifdir) == False:
    os.mkdir(classifdir)
genecorrdir = os.path.join(output_dir,'correlation_with_genes')
if os.path.isdir(genecorrdir) == False:
    os.mkdir(genecorrdir)

pat, db, target, featnames, outcome_dict, genecols = prepare_database(im_type,outcome_name,output_dir)




## 2) VISUALIZE
PCA_images(db,featnames,target,pat,outcome_dict,visudir)
PCA_kD(db,featnames,5,scaling='standard', ret = False)




## 3) UNIVARIATE ANALYSIS
univariate_mannwhitney_bonferroni(db,target,outcome_dict,univdir)




## 4) CLASSIFICATION by CROSS VALIDATION

## Define some algos
# Univariate feature selection
MW_selector = SelectFpr(reducer_MannWhitney,alpha=0.05) # /len(featnames)
LR_selector = SelectFpr(reducer_LogisticRegression,alpha=0.05) 
# Sparse Group Lasso 
# L1_ratio=0 for group lasso, =1 for lasso ; alpha : regularization strength
# scale_l2_by : L2 group penalty
group_indices = get_indices_of_GroupedFeatures(attribute_groups_to_features(featnames,genecols))
sgl_model = LogisticSGL(l1_ratio=0.6, alpha=1, groups=group_indices, scale_l2_by='group_length', fit_intercept=True, max_iter=10000, tol=1e-07, warm_start=False, verbose=1, suppress_solver_warnings=False, include_solver_trace=False)
sgl_selector = SelectFromModel(sgl_model)
# Logistic Regression
logreg = LogisticRegression(solver='liblinear',C=1,max_iter=1000) 
# Store in dictionary 
algos = {'MW_selector':MW_selector,'LR_selector':LR_selector,'SGL_model':sgl_model,'Logreg_classifier':logreg }


## Define pipelines
nested_pipes = Pipeline([
    ('scaler', PipelineHelper([
        ('std', StandardScaler()),
        ('robust', RobustScaler())
            ])
        ),
     ('reducer', PipelineHelper([
         ('univ_LR', LR_selector),
         #('mw', MW_selector),
         ('RF_selector', SelectFromModel(RandomForestClassifier(class_weight = 'balanced_subsample',n_estimators=200))), #,threshold=-np.inf,max_features=15
         #('RFE_selector',RFE(RandomForestClassifier(class_weight = 'balanced_subsample'),n_features_to_select=0.1)),
         #('sparse_group_lasso', sgl_selector)
             ])
         ),
    ('classifier', PipelineHelper([
        ('logreg',LogisticRegression(solver='liblinear',C=0.5,max_iter=5000,class_weight='balanced')),
        #('RF',RandomForestClassifier(class_weight = 'balanced_subsample',n_estimators=200)), #,max_features=4
        #('SVC',SVC(probability=True, kernel='linear',class_weight='balanced'))
            ])
        )
    ])

nested_search_space = {
    'scaler__selected_model': nested_pipes.named_steps['scaler'].generate({

        }),
    'reducer__selected_model': nested_pipes.named_steps['reducer'].generate({
        #'univ_LR__alpha': [0.05], 
        #'mw__alpha':[0.05,0.05/len(featnames)], 
        #'RF_selector__max_features': [10],
        #'RF_selector__estimator__max_features': ['auto',2],
        #'RFE_selector__n_features_to_select': [0.1,2],
        #'sparse_group_lasso__max_features': [5,10,None],
        #'sparse_group_lasso__estimator__l1_ratio': [0.2,0.5,0.8], 
        #'sparse_group_lasso__estimator__alpha': [0.1,0.5,0.8],
        }),
    'classifier__selected_model': nested_pipes.named_steps['classifier'].generate({
        'logreg__C':[0.1,0.5],
        #'logreg__solver': ['liblinear','sag'],
        #'RF__max_features': ['auto',2],
        #'SVC__kernel': ['rbf','sigmoid'],
        })
    }


## Classification
write_pipeline([nested_pipes,nested_search_space],algos,classifdir)
tprs = nested_cv(nested_pipes,nested_search_space,db,featnames,pat,target,genecols,classifdir,inner_nb=5,outer_nb=5,n_repeat=nb_repetition)
cv_plot(db,pat,target,outcome_name,outcome_dict,tprs,classifdir)



## 5) SPEARMAN CORRELATION BETWEEN DEG AND RADIOMIC FEATURES (correlation above 0.5 and pv < 0.05)
# Get intersection database for images and genes
cross_pat, cross_db, cross_target, cross_featnames, cross_outcome_dict, cross_genes = prepare_database(im_type+'_genes',outcome_name,genecorrdir)
cross_db[cross_featnames] = StandardScaler().fit_transform(cross_db[cross_featnames])
cross_radio = [feat for feat in cross_featnames if feat not in cross_genes]
cross_radio = adapt_featnames_list(cross_radio)
cross_featnames = cross_radio+cross_genes
cross_db.columns = ['PatID']+cross_featnames
# Get Spearman correlations and pvalues
all_correlations = cross_db.loc[:,cross_featnames].corr(method='spearman')
pval = cross_db.loc[:,cross_featnames].corr(method=lambda x, y: spearmanr(x, y)[1]) - np.eye(*all_correlations.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05] if x<=t]))
corr_p_test = all_correlations.round(2).astype(str) + p.astype(str)
# Get largest correlations
abs_correlations = all_correlations.abs()
subcorr = all_correlations.loc[cross_genes,cross_radio]
print("Best correlation = "+str(np.amax(subcorr.values)))
# Get all genes and radiomic features that have significant Spearman correlations with absolute value larger than 0.5
threshold = 0.80
npat = len(cross_pat)
best_genes = [gene for gene in cross_genes if np.sum(abs_correlations.loc[gene,cross_radio]>threshold)*np.sum(pval.loc[gene,cross_radio]<0.05)>0]
best_feat = [feat for feat in cross_radio if np.sum(abs_correlations.loc[best_genes,feat]>threshold)*np.sum(pval.loc[best_genes,feat]<0.05)>0]

corr_df = pd.DataFrame(columns=['Feature','Gene','Spearman Correlation','p-value','95%-confidence_interval'])
for feat in best_feat:
     for gene in best_genes:
         if abs_correlations.loc[gene,feat]>threshold and pval.loc[gene,feat]<0.05:
             corr_value = all_correlations.loc[feat,gene].round(2)
             stderr = 1.0 / math.sqrt(npat - 3)
             delta = 1.96 * stderr
             lower = math.tanh(math.atanh(corr_value) - delta)
             upper = math.tanh(math.atanh(corr_value) + delta)
             corr_df.loc[len(corr_df)] = [feat,gene,corr_value,pval.loc[gene,feat].round(5),(round(lower,2),round(upper,2))]
corr_df = corr_df.sort_values(by='p-value')
corr_df.to_csv(genecorrdir+'/'+im_type+'_genes_correlations.csv')

