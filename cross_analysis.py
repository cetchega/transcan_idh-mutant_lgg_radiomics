#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 22:46:54 2023

- Cross-analysis 
    For each biological signature, Spearman correlation with each imaging feature and signature.
    Build cross-signatures : standardized biological score + standardized radiomic feature ; median-split ; log-rank ; 
    Propensity score matching and Kaplan-Meier. 

@author: cetchega
"""
##### IMPORTS #####
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
import pandas as pd
from math import log2
from sklearn.preprocessing import StandardScaler
from lifelines import KaplanMeierFitter
import matplotlib.patches as mpatches

from utils_general import prepare_database, adapt_featname, adapt_featnames_list
import utils_propensity_score_matching

matplotlib.rcParams['axes.facecolor'] = 'white'
matplotlib.rcParams['axes.edgecolor'] = 'black'


def plot_km_curve(feat,pv,T,C,survival_group_for_feat,savingdir,filename):
        matplotlib.rcParams['axes.facecolor'] = 'white'
        matplotlib.rcParams['axes.edgecolor'] = 'black'
        
        kmf = KaplanMeierFitter()
        palette = ["windows blue","tomato"]
        plt_palette = ["xkcd:"+name for name in palette]
        sns.set_palette(sns.xkcd_palette(palette))
        ##SET UP PLOT
        f = plt.figure(figsize=(10,6))
        ax = f.add_subplot(111)
        if pv < 0.001:
            ax.text(0.5,.05,'$p<0.001$',fontsize=18)
        else:
            ax.text(0.5,.05,'$p=%0.3f$' % pv,fontsize=18)
        
        if feat in featnames: # Single radiomics feature
            plt.title(r'$'+ adapt_featname(feat)+'$')
        elif feat[feat.find('+')+2:] in featnames: # Gene or proteomics cross-signature
            plt.title(r''+feat[:feat.find('+')+2]+'$'+ adapt_featname(feat[feat.find('+')+2:])+'$')
        elif feat[::-1][0:feat[::-1].find('+')-1][::-1] in featnames: # Genes+Proteomics cross-signature ; Search from + from backward
            feat_start = feat[:feat.find(' ')+1]+'Signature + '
            feat_end = feat.partition(feat_start)[-1]
            plt.title(r''+feat_start+'$'+ adapt_featname(feat_end)+'$')
        elif feat not in featnames:
            plt.title(r''+ feat+'') 
        else:
            plt.title(r''+feat[:feat.find('+')+2]+'$'+ adapt_featname(feat[feat.find('+')+2:])+'$')
        
        plt.xlabel('Time (years)')
        plt.ylabel('Recurrence-free probability')
        sns.set_context("talk")
        d={} #to store the models
        i=0
        max_surv=0        
        plot_list = []
        ## Curve of Feature
        #loop through segmentation variable, plot on same axes
        for segment in sorted(survival_group_for_feat.unique()): 
            ix = (survival_group_for_feat == segment)
            d['kmf{}'.format(i+1)] = kmf.fit(T.loc[ix],C.loc[ix], label=segment)
            ax = kmf.plot(ax=ax, show_censors=True, ci_alpha=0.1, censor_styles={'ms': 6, 'marker': 's'})
            ax.set_xlim([T.min(),T.max()])
            ax.set_ylim([0,1])
            # Median survival 
            med_surv_time = np.amin([kmf.median_survival_time_,np.amax(T.values)])
            plt.vlines(med_surv_time,0,0.5,[plt_palette[i]],'dashed')
            max_surv = np.amax([max_surv,med_surv_time])
            i+=1
            plot_list.append(ax)
        plt.hlines(0.5,0,max_surv,["grey"],'dashed')
        ##LEGEND
        #override default legend
        patches = [ 
                    mpatches.Patch(color="xkcd:windows blue", label= r' Smaller score'),
                    mpatches.Patch(color="xkcd:tomato", label= r' Larger score')
                  ]
        plt.legend(handles=[patches[0],patches[1]],bbox_to_anchor=(0,1,0,-1.2),loc='upper left');
        plt.xlabel('Time (years)')
        plt.tight_layout()
        f.savefig(os.path.join(savingdir,filename+feat+'.pdf'))
        return None
    
def compute_bio_signatures(biotype,db,biocols):
    aux_df = pd.DataFrame()
    aux_df['PatID'] = db['PatID']
    if biotype == 'genes':
        sum_g1=db['KRT19'].sum()
        sum_g2=db['SCRT2'].sum()
        sum_g3=db['RUNX3'].sum()
        
        krt19_scaler = StandardScaler()
        scrt2_scaler = StandardScaler()
        runx3_scaler = StandardScaler()
        db['KRT19_norm'] = krt19_scaler.fit_transform(db['KRT19'].apply(lambda x: log2( (x+0.5)/(1+sum_g1) *1e6)).values.reshape(-1, 1))
        db['SCRT2_norm'] = scrt2_scaler.fit_transform(db['SCRT2'].apply(lambda x: log2( (x+0.5)/(1+sum_g2) *1e6)).values.reshape(-1, 1))
        db['RUNX3_norm'] = runx3_scaler.fit_transform(db['RUNX3'].apply(lambda x: log2( (x+0.5)/(1+sum_g3) *1e6)).values.reshape(-1, 1))
        aux_df['Signature'] = db[['KRT19_norm','SCRT2_norm','RUNX3_norm']].mean(axis=1).values
    elif biotype == 'proteomics':
        sums = [db[prot].sum() for prot in biocols]
        for i,prot in enumerate(biocols):
            aux_df[prot+'_norm'] = StandardScaler().fit_transform( db[prot].apply(lambda x: log2( (x+0.5)/(1+sums[i]) *1e6)).values.reshape(-1, 1) )
        aux_df['Signature'] = aux_df[[prot+'_norm' for prot in biocols]].mean(axis=1).values
    return aux_df




##### PROG #####

## 0) INITIALIZATION 
feat_type = 'PET' # or MRI
outcome_name = 'survival'
radiomic_signatures_filename = feat_type+'_signatures.csv'
genes_of_interest = ['KRT19','SCRT2','RUNX3']
proteo_of_interest = ['Q9UBB4','Q15056','P06756','A0A0D9SF30']




## 1) CROSS-SIGNATURES

for biotype in ['genes','proteomics']: 
    print('----- Cross-Signatures with '+biotype+ ' -----')
    im_type = biotype+'_'+feat_type
    
    output_dir = os.path.join('results',outcome_name+'_'+feat_type,'cross_signatures')
    if os.path.isdir(output_dir) == False:
        os.mkdir(output_dir)
        
    cross_signatures_dir = output_dir
    
    output_dir = os.path.join(output_dir,im_type)
    if os.path.isdir(output_dir) == False:
        os.mkdir(output_dir)
    
    bio_of_interest = genes_of_interest*(biotype=='genes') + proteo_of_interest*(biotype=='proteomics')
    
    # Radiomic and Bio database 
    pat, db, target, featnames, outcome_dict, biocols = prepare_database(im_type,outcome_name,output_dir,specific_bio_features=bio_of_interest)
    numerical_features = [feat for feat in featnames if feat not in ['Sex','Grade']+biocols]
    db.loc[:,numerical_features] = StandardScaler().fit_transform(db.loc[:,numerical_features])
    db.columns = ['PatID']+adapt_featnames_list(featnames)
    
    # Survival database
    survival_df = target[['PFS','RECURRENCE']]
    survival_df.loc[:,'PFS'] = survival_df['PFS'].apply(lambda time: time/12)

    # Radiomic signatures 
    radiomic_signature_df = pd.read_csv(os.path.join('databases',radiomic_signatures_filename),index_col=0)
    radiomic_signature_df = radiomic_signature_df[radiomic_signature_df['PatID'].isin(pat)].reset_index(drop=True)
    
    # Biological signature
    aux_df = compute_bio_signatures(biotype,db,bio_of_interest)
    if biotype == 'genes':
        gene_signature_df = aux_df[['PatID','Signature']]
    elif biotype == 'proteomics':
        proteo_signature_df = aux_df[['PatID','Signature']]

    # Show histogram of Bio Signature 
    plt.figure()
    aux_df['Signature'].hist()

    # Merge the dataframes of signatures and single features
    db = db.drop(columns=bio_of_interest)
    merged_radiomic_bio_signatures = db.merge(radiomic_signature_df, how='outer', on='PatID')
    # Cross-signatures are not standardized 
    merged_radiomic_bio_signatures = merged_radiomic_bio_signatures.merge(aux_df[['PatID','Signature']], how='outer', on='PatID')
    print('Shape of the merged database : ', merged_radiomic_bio_signatures.shape)
    
    signatures_features_names = list(merged_radiomic_bio_signatures.columns.drop(['PatID','Signature']).values)
    signatures_features_names_with_signature = list(merged_radiomic_bio_signatures.columns.drop('PatID').values)

    ## Create dataframe of cross-signatures
    cross_signatures_array = np.array([merged_radiomic_bio_signatures[sig].values + merged_radiomic_bio_signatures['Signature'].values for sig in merged_radiomic_bio_signatures.columns.values if (sig != 'PatID') and (sig !='Signature')])
    cross_signatures_df = pd.DataFrame(cross_signatures_array.T)
    cross_signatures_df.columns = [biotype.title() + ' Signature + ' + sig for sig in merged_radiomic_bio_signatures.columns.values if (sig != 'PatID') and (sig != 'Signature')]
    cross_signatures_df[biotype.title() + ' Signature'] = merged_radiomic_bio_signatures['Signature'].values
    cross_signatures_df['PatID'] = merged_radiomic_bio_signatures['PatID']

    ## Get survival groups for each patient and according to each cross-signature
    cross_signatures_namelist = list(cross_signatures_df.columns.values)
    cross_signatures_namelist.remove('PatID')
    cross_signatures_logrank_pvs = np.zeros(len(cross_signatures_namelist))

    relevant_signatures_features_names = []
    relevant_radiomic_signatures_logrank_pvs = []
    relevant_cross_signatures = []

    cross_signatures_survival_groups_df = pd.DataFrame()
    for cross_sig in cross_signatures_namelist:
        med = cross_signatures_df[cross_sig].median()
        cross_signatures_survival_groups_df.loc[:,cross_sig] = 1*(cross_signatures_df[cross_sig]>med)
    cross_signatures_survival_groups_df['PatID'] = cross_signatures_df['PatID'].values
    
    # Get target for ind feat to check 
    radiomic_signatures_survival_groups_df = pd.DataFrame()
    for feat in signatures_features_names:
        med = merged_radiomic_bio_signatures[feat].median()
        radiomic_signatures_survival_groups_df.loc[:,feat] = 1*(merged_radiomic_bio_signatures[feat]>med)
    radiomic_signatures_survival_groups_df['PatID'] = merged_radiomic_bio_signatures['PatID'].values






    
    ## 2) PROPENSITY SCORE MATCHING and the features mentioned in the manuscript.
    output_dir = os.path.join(output_dir,'propensity_score_matching')
    if os.path.isdir(output_dir) == False:
        os.mkdir(output_dir)
    out_cross = os.path.join(output_dir,'cross')
    if os.path.isdir(out_cross) == False:
        os.mkdir(out_cross)
    out_single = os.path.join(output_dir,'single')
    if os.path.isdir(out_single) == False:
        os.mkdir(out_single)
        
    # set the figure shape and size of outputs
    sns.set(rc={'figure.figsize':(10,8)}, font_scale = 1.3)
    covar = ['EOR','Age','ADJUVANT Rx','Sex','PET','Histology','Grade'] * (feat_type=='MRI') + ['EOR','Age','ADJUVANT Rx','Sex','Histology','Grade'] * ('PET' in feat_type)
    target['PatID']= pat
    utils_propensity_score_matching.PropScoreMatching(cross_signatures_survival_groups_df,target,feat_type,covar,20,out_cross)
    utils_propensity_score_matching.PropScoreMatching(radiomic_signatures_survival_groups_df,target,feat_type,covar,20,out_single)





