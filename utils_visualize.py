#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 23:08:49 2023

Dimension reduction 

@author: cetchega
"""
 
##### IMPORTS #####

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.gridspec import SubplotSpec
import seaborn as sns

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, RobustScaler


##### FUNCTIONS #####

def PCA_images(db,feat,y,patindex,outcome_dict,savingdir):
    """
    Perform PCA in dimensions 2 and 3 for both standard and robust data scaling. 
    Return None.
    
    Arguments : 
    db : features dataframe
    feat : list of feature names
    y : output dataframe 
    patindex : Series of pat ID
    outcome_dict : dictionary to show outcomes
    savingdir : directory
    """
    X_Std = pd.DataFrame(StandardScaler().fit_transform(db[feat]), columns=feat)
    X_Rob = pd.DataFrame(RobustScaler().fit_transform(db[feat]), columns=feat)
    dim = 3
    target_name = y.name
    
    rows = 2
    cols = 2
    
    ##----------------------##
    ##-- STANDARD SCALING --##
    ##----------------------##
    X = X_Std
    
    pca = PCA(n_components=dim)
    pca_result = pca.fit_transform(X.values)
    colname=['PCA'+str(i) for i in range(1,dim+1)]
    X_out_std = pd.DataFrame(pca_result[:,0:dim],columns=colname) 
    
    print('----- PCA -----')
    print('Standard Scaling')
    print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))

    fig = plt.figure(figsize = (12,12))
    ## Subplot 1 : 2D
    ax = fig.add_subplot(rows, cols, 1)
    data=pd.concat([X_out_std[['PCA1','PCA2']], y], axis=1)
    if target_name != 'PFS':
        hue_regplot(data=data, x='PCA1', y='PCA2', hue=target_name, outcome_dict = outcome_dict, ax=ax,fit_reg=False)
    else:
        data.plot.scatter(x='PCA1', y='PCA2', c = 'PFS',ax=ax, cmap = cm.get_cmap('Spectral'))
    ax.set_xlabel('PCA1 ({:.2%})'.format(pca.explained_variance_ratio_[0]))
    ax.set_ylabel('PCA2 ({:.2%})'.format(pca.explained_variance_ratio_[1]))
    ax.legend(loc=1)
    ## Subplot 2 : 3D 
    ax = fig.add_subplot(rows, cols, 2, projection='3d')
    if target_name != 'PFS':
        ax.scatter(
                xs=X_out_std['PCA1'].iloc[y[y==0].index], 
                ys=X_out_std['PCA2'].iloc[y[y==0].index], 
                zs=X_out_std['PCA3'].iloc[y[y==0].index], 
                c='blue', 
                label='0',
                )
        ax.scatter(
                xs=X_out_std['PCA1'].iloc[y[y==1].index], 
                ys=X_out_std['PCA2'].iloc[y[y==1].index], 
                zs=X_out_std['PCA3'].iloc[y[y==1].index], 
                c='red', 
                label='1',
                )
        ax.legend()
    else:
        ax.scatter(xs = X_out_std['PCA1'], ys = X_out_std['PCA2'], zs = X_out_std['PCA3'],
                   c = y, cmap = cm.get_cmap('Spectral'))
    ax.set_xlabel('PCA1 ({:.2%})'.format(pca.explained_variance_ratio_[0]))
    ax.set_ylabel('PCA2 ({:.2%})'.format(pca.explained_variance_ratio_[1]))
    ax.set_zlabel('PCA3 ({:.2%})'.format(pca.explained_variance_ratio_[2]))
    
    # Check correlations
    for i in range(dim):
        index_corr = np.nonzero(pca.components_[i]>0.6)[0]
        if len(index_corr) >0:
            for j in range(len(index_corr)):
                print('For feature '+feat[index_corr[j]]+', correlation with PC'+str(i+1)+' is equal to '+str(pca.components_[i,index_corr[j]]))
    
    ##----------------------##
    ##--- ROBUST SCALING ---##
    ##----------------------##
    X = X_Rob
    
    pca = PCA(n_components=dim)
    pca_result = pca.fit_transform(X.values)
    colname=['PCA'+str(i) for i in range(1,dim+1)]
    X_out_rob = pd.DataFrame(pca_result[:,0:dim],columns=colname) 
    
    print('Robust Scaling')
    print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))
    
    data=pd.concat([X_out_rob[['PCA1','PCA2','PCA3']], y], axis=1)
    ## Subplot 3 : 2D
    ax = fig.add_subplot(rows, cols, 3)
    if target_name != 'PFS':
        hue_regplot(data, x='PCA1', y='PCA2', hue=target_name,outcome_dict = outcome_dict, ax=ax,fit_reg=False)
    else:
        data.plot.scatter(x='PCA1', y='PCA2', c = 'PFS',ax=ax, cmap = cm.get_cmap('Spectral') )
    ax.set_xlabel('PCA1 ({:.2%})'.format(pca.explained_variance_ratio_[0]))
    ax.set_ylabel('PCA2 ({:.2%})'.format(pca.explained_variance_ratio_[1]))
    ax.legend(loc=2)

    ## Subplot 4 : 3D 
    ax = fig.add_subplot(rows, cols, 4, projection='3d')
    if target_name != 'PFS':
        ax.scatter(
                xs=X_out_rob['PCA1'].iloc[y[y==0].index], 
                ys=X_out_rob['PCA2'].iloc[y[y==0].index], 
                zs=X_out_rob['PCA3'].iloc[y[y==0].index], 
                c='blue', 
                label='0',
                )
        ax.scatter(
                xs=X_out_rob['PCA1'].iloc[y[y==1].index], 
                ys=X_out_rob['PCA2'].iloc[y[y==1].index], 
                zs=X_out_rob['PCA3'].iloc[y[y==1].index], 
                c='red', 
                label='1',
                )
        ax.legend()
    else:
        ax.scatter(xs = X_out_rob['PCA1'], ys = X_out_rob['PCA2'], zs = X_out_rob['PCA3'],
                   c = y , cmap = cm.get_cmap('Spectral'))
    ax.set_xlabel('PCA1 ({:.2%})'.format(pca.explained_variance_ratio_[0]))
    ax.set_ylabel('PCA2 ({:.2%})'.format(pca.explained_variance_ratio_[1]))
    ax.set_zlabel('PCA3 ({:.2%})'.format(pca.explained_variance_ratio_[2]))

    grid = plt.GridSpec(rows, cols)
    create_subtitle(fig, grid[0, ::], 'Standard Scaling')
    create_subtitle(fig, grid[1, ::], 'Robust Scaling')
    fig.tight_layout()
    plt.show()
    fig.savefig(savingdir+'/PCA.pdf')
    
    # Check correlations
    for i in range(dim):
        index_corr = np.nonzero(pca.components_[i]>0.6)[0]
        if len(index_corr) >0:
            for j in range(len(index_corr)):
                print('For feature '+feat[index_corr[j]]+', correlation with PC'+str(i+1)+' is equal to '+str(pca.components_[i,index_corr[j]]))

    return None


def PCA_kD(db,feat,k,scaling='standard', ret = False): 
    """
    Perform PCA in dimension for standard or robust data scaling. 
    Return None. 
    If ret=True, returns the corresponding dataframe.  
    
    Arguments : 
    db : features dataframe
    feat : list of feature names
    k : PCA dimension 
    scaling : "standard" (default) or "robust"
    ret : return dataframe (default = False)
    
    """
    if scaling =='standard':
        X = pd.DataFrame(StandardScaler().fit_transform(db[feat]), columns=feat)
    elif scaling =='robust':
        X = pd.DataFrame(RobustScaler().fit_transform(db[feat]), columns=feat)
    
    pca = PCA(n_components=k)
    pca_result = pca.fit_transform(X.values)
    print('----- PCA with ',str(k),' dimensions and '+ str(scaling)+' scaling -----')
    print('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))

    if ret == True:
        colname=['PCA'+str(i) for i in range(1,k+1)]
        return pd.DataFrame(pca_result[:,0:k],columns=colname) 
    else:
        return None

def hue_regplot(data, x, y, hue, outcome_dict, palette=None, **kwargs):
    regplots = []
    levels = data[hue].unique()
    ## For final images labels = {0:r'$PET^{neg}$', 1:r'$PET^{pos}$'}
    if palette is None:
        default_colors = cm.get_cmap('tab10')
        palette = {k: default_colors(i) for i, k in enumerate(levels)}
    for key in levels:
        regplots.append(
            sns.regplot(
                x=x,
                y=y,
                data=data[data[hue] == key],
                color=palette[key],
                label = outcome_dict[key], # key ; labels[key]
                **kwargs
            )
        )
    return regplots


def create_subtitle(fig: plt.Figure, grid: SubplotSpec, title: str):
    "Sign sets of subplots with title"
    row = fig.add_subplot(grid)
    # the '\n' is important
    row.set_title(f'{title}\n', fontweight='semibold', fontsize = '16')
    # hide subplot
    row.set_frame_on(False)
    row.axis('off')