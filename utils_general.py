#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 11:15:15 2023

@author: cetchega
"""
##### IMPORTS #####
import numpy as np
import pandas as pd

##### FUNCTIONS #####

def open_genes_files(outcome_name):
    if outcome_name == 'ColdDiffuse':
        return pd.read_csv("databases/cd_genes.csv").rename(columns = {'Unnamed: 0':'PatID'})
    elif outcome_name == 'AstroOligo':
        return pd.read_csv("databases/ao_genes.csv").rename(columns = {'Unnamed: 0':'PatID'})
    elif outcome_name == 'survival':
        return pd.read_csv("databases/survival_genes.csv").rename(columns = {'Unnamed: 0':'PatID'})

def prepare_database(im_type,outcome_name,savingdir,specific_bio_features=[]):
    biocols = []
    ## Features database
    if 'PET' in im_type:
        # Radiomics features
        db_pet = pd.read_csv('databases/pyradiomics_output_PET.csv',index_col=0)
        db_pet = db_pet.drop(columns = [item for item in db_pet.columns if (item.startswith("diagnostics") or item in ['Image','Mask'])])
        col_pet = db_pet.columns.drop('PatID').values
        db_pet.columns = ['PatID'] + ['Pet_'+ name for name in col_pet]
        # Other PET features
        db_pet_other = pd.read_csv('databases/PET_other_features.csv',index_col=0)
        # Merge on Patient ID
        db = db_pet.merge(db_pet_other, how='inner', on='PatID')
        if 'MRI' in im_type:
            db_irm = pd.read_csv('databases/pyradiomics_output_MRI.csv',index_col=0)  
            db_irm = db_irm.drop(columns = [item for item in db_irm.columns if (item.startswith("diagnostics") or item in ['Image','Mask'])])
            col_irm = db_irm.columns.drop('PatID').values
            db_irm.columns = ['PatID'] + ['Flair_'+ name for name in col_irm]
            # Merge on Patient ID
            db = db.merge(db_irm, how='inner', on='PatID')
        if 'genes' in im_type:
            db_genes = open_genes_files(outcome_name)
            if specific_bio_features == []:
                specific_bio_features = list(db_genes.columns.drop('PatID').values)
            db_genes = db_genes.loc[:,['PatID']+specific_bio_features]
            biocols = specific_bio_features
            # Merge on Patient ID
            db = db.merge(db_genes, how='inner', on='PatID') # Intersection of keys
        if 'proteomics' in im_type:
            proteo_df = pd.read_csv("databases/proteomics.csv").rename(columns = {'Unnamed: 0':'PatID'})
            proteo_df = proteo_df.loc[:,['PatID']+specific_bio_features]
            biocols = specific_bio_features
            # Merge on Patient ID
            db = db.merge(proteo_df, how='inner', on='PatID') # Intersection of keys
    elif 'MRI' in im_type:
        db = pd.read_csv('databases/pyradiomics_output_MRI.csv',index_col=0)  
        db = db.drop(columns = [item for item in db.columns if (item.startswith("diagnostics") or item in ['Image','Mask'])])
        col_irm = db.columns.drop('PatID').values
        db.columns = ['PatID'] + ['Flair_'+ name for name in col_irm]
        if 'genes' in im_type:
            db_genes = open_genes_files(outcome_name)
            if specific_bio_features == []:
                specific_bio_features = list(db_genes.columns.drop('PatID').values)
            db_genes = db_genes.loc[:,['PatID']+specific_bio_features]
            biocols = specific_bio_features
            # Merge on Patient ID
            db = db.merge(db_genes, how='inner', on='PatID') # Intersection of keys
        if 'proteomics' in im_type:
            proteo_df = pd.read_csv("databases/proteomics.csv").rename(columns = {'Unnamed: 0':'PatID'})
            proteo_df = proteo_df.loc[:,['PatID']+specific_bio_features]
            biocols = specific_bio_features
            # Merge on Patient ID
            db = db.merge(proteo_df, how='inner', on='PatID') # Intersection of keys   
    # Numeric data type
    for i in range(0, len(db.columns)):
        db.iloc[:,i] = pd.to_numeric(db.iloc[:,i], errors='ignore')
    
    featnames = list(db.columns.drop('PatID').values)
    pat_db = db['PatID'].values
        
    ## Target
    if outcome_name in ['ColdDiffuse','AstroOligo']:
        target_df = pd.read_csv('databases/target.csv',index_col=0)
    elif outcome_name == 'CODA':
        target_df = pd.read_csv('databases/target.csv',index_col=0)
        target_df['CODA'] = 1*(target_df['COLD/DIFFUSE']==0).values*(target_df['ASTRO/OLIGO']==1).values + 2*(target_df['COLD/DIFFUSE']==1).values*(target_df['ASTRO/OLIGO']==0).values -1 
        target_df = target_df[target_df['CODA'] != -1].reset_index(drop=True)
    elif outcome_name.startswith('survival'):
        target_df = pd.read_csv('databases/survival_target.csv',index_col=0)
    ## Pat
    pat_target = target_df['PatID'].values
    pat = np.intersect1d(pat_db,pat_target)
    db = db[db['PatID'].isin(pat)].reset_index(drop=True)
    target_df = target_df[target_df['PatID'].isin(pat)].reset_index(drop=True)
    
    if outcome_name == 'ColdDiffuse':
        target = target_df['COLD/DIFFUSE'] 
        nb1 = sum(target.values)
    elif outcome_name == 'AstroOligo':
        target = target_df['ASTRO/OLIGO'] 
        nb1 = sum(target.values)
    elif outcome_name == 'CODA':
        target = target_df['CODA']
        nb1 = sum(target.values)
    else:
        target = target_df[['PFS','RECURRENCE']] #,'Censored'
        nb1 = sum(target['RECURRENCE'].values)
        
    ## Summary 
    nb_feat = db[featnames].shape[1]
    nb_pat = db[featnames].shape[0]
    nb0 = nb_pat - nb1
    with open(savingdir+'/data_summary.txt', 'w') as f:
        f.write("Analysis of outcome {}.\n".format(outcome_name))
        f.write("There are {} features for {} patients.\n".format(nb_feat,nb_pat))
        f.write("There are {} pat with outcome 1, {} with outcome 0.\n".format(nb1,nb0))
        f.write("If outcome is survival, then outcome 1 is for recurrence, but the outcome is not binarized in the analysis.")
    
    if outcome_name == 'ColdDiffuse':
        outcome_dict = {0:r'$PET^{neg}$',1:r'$PET^{pos}$'}
    elif outcome_name == 'AstroOligo':
        outcome_dict = {0:'Astrocytoma', 1:'Oligodendroglioma'}
    elif outcome_name == 'CODA':
        outcome_dict = {0:r'$PET^{neg}$ Oligodendroglioma',1:r'$PET^{pos}$ Astrocytoma'}
    else:
        outcome_dict = {}
        
    return pat, db, target, featnames, outcome_dict, biocols



def adapt_featname(feat):
    other_dict = {'Grade':'Grade','Age':'Age','HI':'HetIndex','NHI':'HetIndex_{NORM}','WHI':'Weighted HetIndex','NWHI':'Weighted HetIndex_{NORM}','SKEW':'HetIndex_{SKEWNESS}','KURT':'HetIndex_{KURTOSIS}','rad_score':'Radiomic Score'}
    suv_feat = ['SUVmax', 'SUVmean', 'SUVNorm', 'SUVR','MTB','MTV','TLG']
    name = feat

    if name.startswith('Pet_original'):
        name = name[13:]
        t_ind = np.char.find(name,'_')
        typename = + (name[0:t_ind]!='shape')*name[0:t_ind].upper() + 'MORPH'*(name[0:t_ind]=='shape')
        feat_sub = 'PET,'+typename
        newname = name[t_ind+1:]+ '_{'+feat_sub+'}'
    elif name.startswith('Flair_original'):
        name = name[15:]
        t_ind = np.char.find(name,'_')
        typename = + (name[0:t_ind]!='shape')*name[0:t_ind].upper() + 'MORPH'*(name[0:t_ind]=='shape')
        feat_sub = 'MRI,'+ typename
        newname = name[t_ind+1:]+ '_{'+feat_sub+'}'
    elif name.startswith('rad_score'):
        if name[-2:] == 'it': # 0 split
            newname = adapt_featname(name[0:-10]) + ' - 0 split'
        elif name[-2:] not in ['09','95']:
            newname = 'RadiomicScore_{'+name[-3:]+'}'
        else:
            if name[10]=='C': #cox
                newname = 'RadiomicScore_{Cox'+'_{'+name[-2:]+'}}'
            elif name[10]=='p': #10%
                newname = 'RadiomicScore_{pct'+'_{'+name[-2:]+'}}'
            else:
                newname = 'RadiomicScore_{all'+'_{'+name[-2:]+'}}'
    elif (name not in suv_feat) and (name in other_dict.keys()):
        newname = other_dict[name]
    elif '+' in name: # cross-signature
        newname = name.split(' + ')[0] + ' + ' +adapt_featname(name.split(' + ')[1])
    else:
        newname = name
    return newname

def adapt_featnames_list(featn):
    new_featn = []
    for name in featn:
        if ' + ' in name: # cross-signature
            add_dollar = (name.split(' + ')[1].startswith('$')==False)
            new_featn += [r''+name.split(' + ')[0] + ' + ' +'$'*add_dollar + adapt_featname(name.split(' + ')[1]) + '$'*add_dollar]
        elif name in ['KURT','SKEW']:
            add_dollar = ( ('$' in name) ==False)
            new_featn += [r''+ '$'*add_dollar + adapt_featname(name) + '$'*add_dollar]
        elif '_' not in name:
            new_featn += [adapt_featname(name)]
        else:
            add_dollar = ( ('$' in name) ==False)
            new_featn += [r''+ '$'*add_dollar + adapt_featname(name) + '$'*add_dollar]
    return new_featn


