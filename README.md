# Radiomics scripts for IDH-mutant Low Grade Glioma imaging analysis 

## Description
This project is associated with the Glioma-PRD Transcan project ["Multi-parametric analysis of the evolution and progression of low grade glioma"](https://transcan.eu/output-results/funded-projects/glioma-prd.kl), that gave rise to the submitted manuscript called "Multimodal integration of transcriptomics, proteomics, and radiomics improves prediction of recurrence in patients with IDH-mutant Glioma".

Please cite the article if you use the code in your project.
All Copyrights (c) of codes belong to: Christèle Etchegaray, Inria, Univ. Bordeaux, CNRS, IMB, UMR 5251, F-33400 Talence, France.

The code was used and tested on Python 3.7. 

## Usage
Please clone the whole repository to correctly launch the scripts. 

The "databases" folder contains the values that were used in the analysis. 
The "results" folder is empty and will store the results obtained from running the scripts. 
Scripts named utils_*.py contain the functions used in the other scripts. 

The scripts may be used following this order : 
- classification_analysis.py performs classification of patients according to either their PET or histological status
- survival_analysis.py performs a nested cross-validation of CoxNet model to build signatures of the recurrence risk, and test them using Kaplan-Meier analysis and log-rank test with Propensity score matchin.
- cross_analysis.py performs combination of radiomics and molecular features/signatures that are tested on the whole cohort. 
- radiomics_benchmarking.py performs comparison of signatures from the literature on our database.

## Support
christele.etchegaray@inria.fr

## License
Please cite the article if you use the code in your project.
All Copyrights (c) of codes belong to: Christèle Etchegaray, Inria, Univ. Bordeaux, CNRS, IMB, UMR 5251, F-33400 Talence, France.

## Project status
The development of this project is stopped. 
