#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 12:01:31 2023

@author: cetchega
"""

##### IMPORTS #####
#import os
import numpy as np
import pandas as pd
import time
import ast
import os

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.compose import ColumnTransformer
from sklearn.pipeline import make_pipeline
from sklearn.experimental import enable_halving_search_cv 
from sklearn.model_selection import KFold, HalvingGridSearchCV, RepeatedKFold, GridSearchCV
from sklearn.preprocessing import StandardScaler

from lifelines import CoxPHFitter
from sksurv.linear_model import CoxnetSurvivalAnalysis # Without penalty : CoxPHSurvivalAnalysis
#from pysurvival.models.survival_forest import RandomSurvivalForestModel, ConditionalSurvivalForestModel
from sksurv.metrics import integrated_brier_score 
from sksurv.metrics import brier_score 

from lifelines import KaplanMeierFitter
from lifelines.statistics import logrank_test
import matplotlib.patches as mpatches

from utils_general import adapt_featnames_list
import utils_propensity_score_matching

##### FUNCTIONS #####
 
def cox_univ_pv(all_feats,y,X):
    # Univariate Cox 
    l=0
    scores_pv = np.zeros(len(all_feats))
    scores_hr = np.zeros(len(all_feats))
    for feat in all_feats:
        cox_db = y.loc[:,['PFS','RECURRENCE']].copy()
        cox_db.loc[:,feat] = X[feat].values
        
        # # If want to add covariates
        # cox_db.loc[:,'Sex'] = y.loc[:,['Sex']]
        # cox_db.loc[:,'Grade'] = y.loc[:,['Grade']]
        # cox_db.loc[:,'Age'] = StandardScaler().fit_transform(y.loc[:,['Age']])
        
        if feat not in ['Sex','Grade']:
            cox_db.loc[:,feat] = StandardScaler().fit_transform(cox_db.loc[:,[feat]])
    
        cph = CoxPHFitter()
        cph.fit(cox_db,"PFS",event_col="RECURRENCE")
        
        scores_pv[l] = cph.summary.loc[feat,'p']
        scores_hr[l] = cph.summary.loc[feat,'exp(coef)']
        l+=1
    return scores_pv, scores_hr



def cv_Lasso_Cox(db,cofeat_names,pat,target,savingdir,outer_nb=5,n_repeat=5,halving=True):
     # Database
     featnames = [feat for feat in db.columns.values if feat not in ['PatID']+cofeat_names]
     X = db[featnames]
     
     # Covariate and target df for univariate Cox
     cofeat_df = db.loc[:,cofeat_names] # for univariate Cox
     cofeat_df.loc[:,'RECURRENCE'] = target['RECURRENCE'].astype(bool)
     cofeat_df.loc[:,'PFS'] = target['PFS']
     
     E = target['RECURRENCE'].astype(bool)
     T = target['PFS']
     
     ## Alpha in LASSO - parameters
     train_alphas = np.zeros(outer_nb*n_repeat)
     amr = 0.5 # alpha min ratio # MRI : amr = 0.05 and maxiter = 1e9 (amr 0.5)
     # all_imaging : amr = 0.7, maxiter = 0.9
     maxiter = 1e7
     
     ## Scores for all features - univariate cox and cox weights in cv
     univariate_cox_pv = np.zeros((outer_nb*n_repeat,len(featnames)))
     univariate_cox_hr = np.zeros((outer_nb*n_repeat,len(featnames)))
     features_weights_in_cox_cv = np.zeros((outer_nb*n_repeat,len(featnames)))
     
     ## Tuples of Scores for features selected by Lasso+Cox in each external fold
     train_imp_tuples = []
     train_pv_tuples = []
     train_hr_tuples = []
     train_feat_tuples = []
     
     ## Global test scores
     mean_times = np.linspace(0,int(np.max(T)),200)
     test_cic_cox = np.zeros(outer_nb*n_repeat)
     test_ibs_cox = np.zeros(outer_nb*n_repeat)
     test_briers_cox = []
     
     ## Logrank tests
     logrank_pv = np.zeros(outer_nb*n_repeat)
     with open(os.path.join(savingdir,'CrossValidationLogrankPV_median.txt'), 'w') as f:
         f.write('p-values for logrank test from cross-validation ; splitting at median PFS \n')
     
     print('----- {}-fold nested cross-validation with {} repetitions. -----'.format(outer_nb,n_repeat))
     outer_kf = RepeatedKFold(n_splits=outer_nb,n_repeats=n_repeat)
     j=0 # To count nb of outer cv loop
     # Looping through the outer loop, feeding each training set into a GSCV as the inner loop
     for train_index,test_index in outer_kf.split(X,target):
         print(str(j)+'/'+str(outer_nb*n_repeat))
         Xtrain = X.loc[train_index]
         Xtest = X.loc[test_index]
         E_train = E[train_index]
         T_train = T[train_index]
         E_test = E[test_index]
         T_test = T[test_index]
         
         # Prepare E and T for Cox 
         df = pd.concat([E_train, T_train], axis=1) 
         s = df.dtypes
         ytrain = np.array([tuple(x) for x in df.values], dtype=list(zip(s.index, s)))
         
         df = pd.concat([E_test, T_test], axis=1)
         s = df.dtypes
         ytest = np.array([tuple(x) for x in df.values], dtype=list(zip(s.index, s)))
         
         # Time array for Brier scores
         mean_time_test = np.linspace(np.amax([np.min(T_test),np.min(T_train)])+1e-4,np.amin([np.max(T_test),np.max(T_train)])-1e-4,100)
         
         print('----- Univariate Cox analysis on train set -----')
         univariate_cox_pv[j,:], univariate_cox_hr[j,:] = cox_univ_pv(featnames,cofeat_df.iloc[train_index],Xtrain)

         numerical_columns = list(set(featnames) - set(['Sex','Grade']))
         new_features_indices = np.arange(len(featnames))
         new_features = featnames
         
         print('----- Get set of alphas in LASSO -----')
         # Cox pipeline
         preprocessor = ColumnTransformer([
             #('one-hot-encoder', categorical_preprocessor, categorical_columns),
             ('standard_scaler', StandardScaler(), numerical_columns)], remainder ='passthrough')
         coxnet_pipe = make_pipeline(
             preprocessor,
             CoxnetSurvivalAnalysis(l1_ratio=1,max_iter=maxiter,alpha_min_ratio=amr,n_alphas=100)
             )
         
         # Fit on the whole training set to get the set of alphas to consider
         coxnet_pipe.fit(Xtrain, ytrain)
         estimated_alphas = coxnet_pipe.named_steps["coxnetsurvivalanalysis"].alphas_
         #print('We consider ',len(estimated_alphas),' values of alpha.')
         
         print('----- GridSearch -----')
         if halving == True:
             gcv = HalvingGridSearchCV(
                 make_pipeline(preprocessor, CoxnetSurvivalAnalysis(l1_ratio=1,max_iter=maxiter,tol=1e-4)),
                 param_grid={"coxnetsurvivalanalysis__alphas": [[v] for v in estimated_alphas]},
                 error_score=0.5,
                 n_jobs=-1, min_resources=50, factor=2,verbose=0.5,aggressive_elimination=True
                 ).fit(Xtrain, ytrain) # min_resources = "exhaust"
             # By default, cv = 5, and we can not use Shuffle. 

             results = pd.DataFrame(gcv.cv_results_)
             results["params_str"] = results.params.apply(str)
             results.drop_duplicates(subset=("params_str", "iter"), inplace=True)
             mean_scores = results.pivot(
                 index="iter", columns="params_str", values="mean_test_score"
             )
             ax = mean_scores.plot(legend=False, alpha=0.6)
             labels = [
                 f"iter={i}\nn_samples={gcv.n_resources_[i]}\nn_candidates={gcv.n_candidates_[i]}"
                 for i in range(gcv.n_iterations_)
             ]
             ax.set_xticks(range(gcv.n_iterations_))
             ax.set_xticklabels(labels, rotation=45, multialignment="left")
             ax.set_title("Scores of candidates over iterations")
             ax.set_ylabel("mean test score", fontsize=15)
             ax.set_xlabel("iterations", fontsize=15)
             plt.tight_layout()
             plt.show()
             
         else: # GridSearch on alphas 
             time_start = time.clock()
             gcv = GridSearchCV(
                 make_pipeline(preprocessor, CoxnetSurvivalAnalysis(l1_ratio=1,max_iter=maxiter,tol=1e-4)),
                 param_grid={"coxnetsurvivalanalysis__alphas": [[v] for v in estimated_alphas]},
                 cv=KFold(n_splits=5, shuffle=True),
                 error_score=0.5,
                 n_jobs=None,#4
                 verbose=0.9).fit(Xtrain, ytrain)
             time_elapsed = (time.clock() - time_start)
             print('GridSearch Duration:',time_elapsed)
         
         train_alphas[j] = gcv.best_params_["coxnetsurvivalanalysis__alphas"][0]
         
         print('----- Train best model -----')
         ### Train best model 
         cph = make_pipeline(
             preprocessor,
             CoxnetSurvivalAnalysis(l1_ratio=1,max_iter=maxiter,fit_baseline_model=True)
         )
         cph.set_params(**gcv.best_params_)
         cph.fit(Xtrain, ytrain)
         lasso_coefs = cph.named_steps["coxnetsurvivalanalysis"].coef_.ravel()
         # Get coefs 
         features_weights_in_cox_cv[j,new_features_indices] = lasso_coefs
         
         print('----- Get Test scores -----')
         # Get C-index
         test_cic_cox[j] = cph.score(Xtest, ytest)
         # IBS
         survs = cph.predict_survival_function(Xtest)
         preds = np.asarray([[fn(t) for t in mean_time_test] for fn in survs])
         test_ibs_cox[j] = integrated_brier_score(ytrain,ytest,preds,mean_time_test)
         # brier
         test_brier_time, test_brier_score = brier_score(ytrain,ytest,preds,mean_time_test)
         test_briers_cox.append(np.interp(mean_times,test_brier_time,test_brier_score))

         non_zero = np.sum(lasso_coefs != 0) 
         print("Number of non-zero coefficients in external fold: {}".format(non_zero))
         if non_zero != 0:
             lasso_coefs_df = pd.DataFrame(lasso_coefs,index=Xtrain.columns,columns=["coefficient"])
             non_zero_coefs = lasso_coefs_df.query("coefficient != 0")
             coef_order = non_zero_coefs.abs().sort_values("coefficient").index
             
             _, ax = plt.subplots(figsize=(6, 8))
             non_zero_coefs.loc[coef_order].plot.barh(ax=ax, legend=False)
             ax.set_xlabel("coefficient")
             ax.grid(True)
         
         # # Get indices of selected_feats in featnames 
         selected_indices = [i for i in range(len(new_features)) if lasso_coefs[i]!=0]
         selected_features = [new_features[i] for i in selected_indices]
         global_selected_indices = [featnames.index(value) for value in selected_features]
         # Store selected features, weights and Cox pv and hr
         train_feat_tuples.append( tuple(selected_features) )
         train_imp_tuples.append( tuple(lasso_coefs[selected_indices]) )
         train_pv_tuples.append( tuple( univariate_cox_pv[j,global_selected_indices] ) )
         train_hr_tuples.append( tuple( univariate_cox_hr[j,global_selected_indices] ) )
         
         # Compute TEST radiomic score from lasso signature
         sum_weights = np.sum(lasso_coefs[selected_indices])
         if sum_weights != 0:
             rad_aux = np.array( [ Xtest[selected_features[i]].values * lasso_coefs[selected_indices][i] for i in range(len(selected_features))] )
             rad_score = rad_aux.sum(axis=0)/ sum_weights

             km_target = pd.DataFrame()
             med = np.median(rad_score)
             km_target['rad_score_med'] = 1*(rad_score>med)

             targ = km_target['rad_score_med']
             target_index_0 = targ[targ == 0].index
             target_index_1 = targ[targ == 1].index
             
             logrank_results = logrank_test(T_test.iloc[target_index_0],T_test.iloc[target_index_1],E_test.iloc[target_index_0],E_test.iloc[target_index_1])
             logrank_pv[j] = logrank_results.p_value

             with open(os.path.join(savingdir,'CrossValidationLogrankPV_median.txt'), 'a') as f:
                 f.write(str(logrank_results.p_value)+'\n')
         
         j+=1
     # End of for loop
     all_coxnet_weights_df = pd.DataFrame(features_weights_in_cox_cv,columns=featnames)
     all_coxnet_weights_df.to_csv(os.path.join(savingdir,'all_features_weights_Coxnet.csv')) 
     
     all_univcox_pv_df = pd.DataFrame()
     all_univcox_pv_df[featnames] = univariate_cox_pv
     all_univcox_pv_df.to_csv(os.path.join(savingdir,'all_features_pv_UnivCox.csv')) 
     
     all_univcox_hr_df = pd.DataFrame()
     all_univcox_hr_df[featnames] = univariate_cox_hr
     all_univcox_hr_df.to_csv(os.path.join(savingdir,'all_features_hr_UnivCox.csv'))
     
     all_features_perf_df = pd.DataFrame()
     all_features_perf_df['features'] = featnames
     all_features_perf_df['Cox_weights - mean'] = np.mean(features_weights_in_cox_cv,axis=0)
     all_features_perf_df['Cox_weights - std'] = np.std(features_weights_in_cox_cv,axis=0)
     all_features_perf_df['Cox pv - mean'] = np.mean(univariate_cox_pv,axis=0)
     all_features_perf_df['Cox pv - std'] = np.std(univariate_cox_pv,axis=0)
     all_features_perf_df['Cox hr - mean'] = np.mean(univariate_cox_hr,axis=0)
     all_features_perf_df['Cox hr - std'] = np.std(univariate_cox_hr,axis=0)
     all_features_perf_df = all_features_perf_df.sort_values(by='Cox pv - mean', ascending=True).reset_index(drop=True)
     all_features_perf_df.to_csv(os.path.join(savingdir,'all_features_mean_scores.csv')) 
     
     fold_perf_df = pd.DataFrame()
     fold_perf_df['selected_features'] = train_feat_tuples #np.array(train_feat_tuples,dtype=(str,Nf))
     fold_perf_df['Cox_weights'] = train_imp_tuples
     fold_perf_df['Cox_alphas'] = train_alphas
     fold_perf_df['Cox_individual_train_pv'] = train_pv_tuples
     fold_perf_df['Cox_individual_train_HR'] = train_hr_tuples
     fold_perf_df['Cox_test_CI'] = test_cic_cox
     fold_perf_df['Cox_test_IBS'] = test_ibs_cox
     
     with open(os.path.join(savingdir,'prediction_performances.txt'), 'w') as f: # Perf.txt
         f.write('COX IBS score:'+ str(np.mean(test_ibs_cox))+' +/- '+str(np.std(test_ibs_cox))+'\n')
         f.write('COX CI score:'+ str(np.mean(test_cic_cox))+' +/- '+str(np.std(test_cic_cox))+'\n')

     plot_Brier(mean_times,test_briers_cox,np.mean(test_ibs_cox),np.std(test_ibs_cox),savingdir,'CoxNet')
     return fold_perf_df
 
    
def correlation_filter(preselected_feats,Xtrain,saving_dir,fig_prefix='',threshold=0.90):
     ## We keep recursively features having above-threshold correlation for a maximum nb of features 
     to_keep = []
     try:
         candidates = list(preselected_feats.values)
     except:
         candidates = list(preselected_feats)

     ## LOWER FILTER : Keep first features with low correlations with all candidates 
     db = Xtrain[candidates]
     corr_matrix = db.corr(method = "spearman").abs().round(2)
     low_corr_everywhere = [ (np.sum(corr_matrix[column]<=0.85) == len(candidates) -1 ) for column in candidates]
     to_keep += [candidates[i] for i in range(len(candidates)) if low_corr_everywhere[i]==1]
     candidates = [feat for feat in candidates if feat not in to_keep]
     
     # UPPER FILTER
     db = Xtrain[candidates]
     corr_matrix = db.corr(method = "spearman").abs().round(2)
     upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(bool))
     while any(upper>=threshold):
         nb_corr = [np.sum(corr_matrix[column]>=threshold) for column in candidates] # Nb of correlated features for each 
         max_indices = np.argwhere(nb_corr == np.max(nb_corr)).ravel().tolist() # Looking for features correlated with a maximum number
         if len(max_indices) ==1:
             sel_feat = candidates[np.argmax(nb_corr)]
         else:
             score_aux = [np.sum(corr_matrix[candidates[i]][corr_matrix[candidates[i]]>=threshold]) for i in max_indices] # Sum of above-threshold correlations 
             best_index = max_indices[np.argmax(score_aux)]
             sel_feat = candidates[best_index] # Keeping the feature with the most above-threshold correlations
         out_feat = [column for column in candidates if corr_matrix.loc[column,sel_feat]>=threshold]
         
         to_keep.append( sel_feat )
         candidates = [feat for feat in candidates if feat not in out_feat]
         
         db = Xtrain[candidates]
         corr_matrix = db.corr(method = "spearman").abs().round(2)
         upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(bool))
     
     db = Xtrain[to_keep+candidates]
     db.columns = np.array(to_keep+candidates,dtype=str)
     
     if 'LASSO' in fig_prefix:
         plt.figure(figsize=(14,10))
     else:
         plt.figure(figsize=(10,8))
     plot = sns.heatmap(db.corr(method = "spearman"),annot=True,fmt='.2f',annot_kws={"size":8})
     plot.set_xticklabels( plot.get_xticklabels(), ha='center') 
     plot.set_yticklabels( plot.get_yticklabels(), va='center')
     if len(fig_prefix) >0:
         fig = plot.get_figure()
         plt.xticks(rotation=45, ha='right')
         plt.tight_layout()
         fig.savefig(os.path.join(saving_dir,fig_prefix+"_correlations_"+str(threshold)+".pdf"),bbox_inches='tight') 
     return to_keep+candidates

def characterize_univcox_and_lassocox_df(im_type,db,fold_perf_df,feat_perf_df,total_fold_number,analysisdir):
    # Load auxiliary data
    all_coxnet_weights_df = pd.read_csv(os.path.join(analysisdir,'all_features_weights_Coxnet.csv'),index_col=0) 
    all_univcox_pv_df = pd.read_csv(os.path.join(analysisdir,'all_features_pv_UnivCox.csv'),index_col=0) 
    all_univcox_hr_df = pd.read_csv(os.path.join(analysisdir,'all_features_hr_UnivCox.csv'),index_col=0) 

    if im_type == 'MRI':
        for df in [all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df]:
            df.columns = adapt_featnames_list([('Flair_')*(name.startswith('original'))+name for name in df.columns.values])
        
    # Check features selected at least once in coxnet
    coxnet_signatures = fold_perf_df['selected_features']

    coxnet_signatures_features = []
    for tup in coxnet_signatures:
        try:
            coxnet_signatures_features += list(ast.literal_eval(tup))
        except:
            coxnet_signatures_features += list(tup)
    coxnet_signatures_features = np.array(coxnet_signatures_features)
    unique_features_in_coxnet, nb_unique_features_in_coxnet = np.unique(coxnet_signatures_features,return_counts=True)
    # Show the features involved in the highest nb of signatures. 
    indexes = np.flip(nb_unique_features_in_coxnet.argsort())
    unique_features_in_coxnet = unique_features_in_coxnet[indexes]
    nb_unique_features_in_coxnet = nb_unique_features_in_coxnet[indexes]
    if im_type == 'MRI':
        unique_features_in_coxnet = [('Flair_')*(name.startswith('original'))+name for name in unique_features_in_coxnet]
    unique_features_in_coxnet = adapt_featnames_list(unique_features_in_coxnet)
    all_coxnet_weights_df.columns = adapt_featnames_list(list(all_coxnet_weights_df.columns.values))
    all_univcox_pv_df.columns = adapt_featnames_list(list(all_univcox_pv_df.columns.values))
    all_univcox_hr_df.columns = adapt_featnames_list(list(all_univcox_hr_df.columns.values))
        
    print("There are {} different signatures identified by CoxNet".format(coxnet_signatures.nunique()))
    print("There are {} different features involved".format(len(unique_features_in_coxnet)))

    nb_of_feats_to_display = 10

    fig, ax = plt.subplots(figsize=(10,6))
    y_pos = np.arange(nb_of_feats_to_display)
    ax.barh(y_pos, nb_unique_features_in_coxnet[0:nb_of_feats_to_display], align='center')
    ax.set_yticks(y_pos, labels=unique_features_in_coxnet[0:nb_of_feats_to_display])
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Number of Signatures')
    ax.set_title('The '+str(nb_of_feats_to_display)+' most selected\nfeatures in signatures')
    plt.tight_layout()
    plt.savefig(os.path.join(analysisdir,'top_features_in_coxnet_signatures.pdf'))
    plt.show()
    
    # Create df of the features selected at least once in LASSO
    coxnet_selected_features_perf_df = feat_perf_df.loc[feat_perf_df['features'].isin(unique_features_in_coxnet),:]
    coxnet_selected_features_perf_df.loc[:,'selection_frequency'] = np.array( [100/total_fold_number*nb_unique_features_in_coxnet[unique_features_in_coxnet.index(feat)] for feat in coxnet_selected_features_perf_df['features'].values] ) 
    coxnet_selected_features_perf_df = coxnet_selected_features_perf_df.sort_values(by='selection_frequency', ascending=True).reset_index(drop=True)
    coxnet_selected_features_perf_df.to_csv(os.path.join(analysisdir,'lasso_selected_features_mean_scores.csv')) 
    
    #### Make corresponding plots : boxplots of importance scores, pv, hr AND correlation plot 
    ## Selection rate above 10%
    nametag = 'lasso_selection_10%'
    selection_above_10pct = coxnet_selected_features_perf_df[coxnet_selected_features_perf_df['selection_frequency']>10]
    #plot_survival_selectedfeatures_boxplots(all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df,selection_above_10pct,nametag,analysisdir)
    plot_survival_selectedfeatures_boxplots_NEWVERSION(all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df,selection_above_10pct,nametag,analysisdir)
    plot_survival_selected_features_correlations(db,selection_above_10pct.features.values,nametag,analysisdir)

    ## Selection rate above 5%
    nametag = 'lasso_selection_5%'
    selection_above_10pct = coxnet_selected_features_perf_df[coxnet_selected_features_perf_df['selection_frequency']>5]
    plot_survival_selectedfeatures_boxplots_NEWVERSION(all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df,selection_above_10pct,nametag,analysisdir)
    plot_survival_selected_features_correlations(db,selection_above_10pct.features.values,nametag,analysisdir)

    ## In LASSO-COX signature
    nametag = 'lasso_univcox'
    selection_and_univcox = coxnet_selected_features_perf_df[coxnet_selected_features_perf_df['Cox pv - mean']<0.05]
    plot_survival_selectedfeatures_boxplots_NEWVERSION(all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df,selection_and_univcox,nametag,analysisdir)
    plot_survival_selected_features_correlations(db,selection_and_univcox.features.values,nametag,analysisdir)

    
    ## Features selected by UnivCox 
    univcox_selected_features_perf_df = feat_perf_df.loc[feat_perf_df['Cox pv - mean']<0.05,:]
    selection_rate_list = []
    for feat in univcox_selected_features_perf_df['features'].values:
        if feat not in unique_features_in_coxnet:
            selection_rate_list.append(0)
        else:
            selection_rate_list.append(100/total_fold_number*nb_unique_features_in_coxnet[unique_features_in_coxnet.index(feat)])
    univcox_selected_features_perf_df.loc[:,'selection_frequency'] = np.array(selection_rate_list) 
    univcox_selected_features_perf_df = univcox_selected_features_perf_df.sort_values(by='selection_frequency', ascending=True).reset_index(drop=True)
    univcox_selected_features_perf_df.to_csv(os.path.join(analysisdir,'univcox_selected_features_mean_scores.csv')) 

    #### Make corresponding plots : boxplots of importance scores, pv, hr AND correlation plot 
    nametag = 'univcox'
    plot_survival_selectedfeatures_boxplots_NEWVERSION(all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df,univcox_selected_features_perf_df,nametag,analysisdir)
    plot_survival_selected_features_correlations(db,univcox_selected_features_perf_df.features.values,nametag,analysisdir)
    return None

def compute_rs_cox(db,feat_perf_df,features,fig_title,saving_dir):
    sum_weights = np.sum( feat_perf_df.loc[feat_perf_df['features'].isin(features),'Cox_weights'].values ) 
    score_aux = np.array( [ db[feat].values * feat_perf_df.loc[feat_perf_df['features']==feat,'Cox_weights'].values for feat in features] )
    score = score_aux.sum(axis=0)/sum_weights
    
    scorename = fig_title.split()[0]
    if 'with' in fig_title:
        corr_threshold = fig_title.split()[2].split('-')[0]
        filename = 'histogram_'+scorename+'_threshold'+corr_threshold+'.pdf'
        figname = r'$'+scorename+'$ with '+corr_threshold+'-level correlation filtration'
    else:
        filename = 'histogram_'+scorename
        figname = r'$'+fig_title+'$'
    
    plt.figure()
    plt.hist(score,bins=25) 
    plt.title(figname)
    plt.xlabel('Score')
    plt.savefig(os.path.join(saving_dir,filename))
    plt.show()
    return score

def compute_radiomic_scores(im_type,db,fold_perf_df,feat_perf_df,saving_dir):
    feat_perf_df['Cox_weights'] = np.log(feat_perf_df['Cox hr - mean'])
    
    ## RadiomicScore_COX and correlation-reduced scores
    cox_features = feat_perf_df['features'][feat_perf_df['Cox pv - mean']<0.05].values
    cox_features_095 = correlation_filter(cox_features,db,saving_dir,'RadiomicScore_{COX}',threshold=0.95)
    cox_features_09 = correlation_filter(cox_features,db,saving_dir,'RadiomicScore_{COX}',threshold=0.9)
    
    rs_cox = compute_rs_cox(db,feat_perf_df,cox_features,'RadiomicScore_{COX}',saving_dir)
    rs_cox_095 = compute_rs_cox(db,feat_perf_df,cox_features_095,'RadiomicScore_{COX} with 0.95-level correlation filtration',saving_dir)
    rs_cox_09 = compute_rs_cox(db,feat_perf_df,cox_features_09,'RadiomicScore_{COX} with 0.9-level correlation filtration',saving_dir)
    
    rs_cox_df = feat_perf_df[['features','Cox pv - mean','Cox pv - std','Cox hr - mean','Cox_weights']].loc[feat_perf_df['Cox pv - mean']<0.05,:]
    rs_cox_095_indicator = ['yes'*(feat in cox_features_095)+'no'*(feat not in cox_features_095) for feat in cox_features]
    rs_cox_09_indicator = ['yes'*(feat in cox_features_09)+'no'*(feat not in cox_features_09) for feat in cox_features]
    rs_cox_df['in RadiomicScore_COX_0.95'] = np.array(rs_cox_095_indicator)
    rs_cox_df['in RadiomicScore_COX_0.9'] = np.array(rs_cox_09_indicator)
    rs_cox_df.to_csv(os.path.join(saving_dir,'RadiomicScore_COX_composition.csv'))
    
    
    ## RadiomicScore_LASSO,COX and correlation-reduced scores
    lasso_features = feat_perf_df['features'][(feat_perf_df['Lasso_weights - mean']!=0.0) & (feat_perf_df['Cox pv - mean']<0.05) ].values
    lasso_features_095 = correlation_filter(lasso_features,db,saving_dir,'RadiomicScore_{LASSO,COX}',threshold=0.95)
    lasso_features_09 = correlation_filter(lasso_features,db,saving_dir,'RadiomicScore_{LASSO,COX}',threshold=0.9)
    
    rs_lassocox = compute_rs_cox(db,feat_perf_df,lasso_features,'RadiomicScore_{LASSO,COX}',saving_dir)
    rs_lassocox_095 = compute_rs_cox(db,feat_perf_df,lasso_features_095,'RadiomicScore_{LASSO,COX} with 0.95-level correlation filtration',saving_dir)
    rs_lassocox_09 = compute_rs_cox(db,feat_perf_df,lasso_features_09,'RadiomicScore_{LASSO,COX} with 0.9-level correlation filtration',saving_dir)
    
    rs_lassocox_df = feat_perf_df[['features','Lasso_weights - mean','Cox pv - mean','Cox pv - std','Cox hr - mean','Cox_weights']].loc[(feat_perf_df['Lasso_weights - mean']!=0.0) & (feat_perf_df['Cox pv - mean']<0.05),:]
    rs_lassocox_095_indicator = ['yes'*(feat in lasso_features_095)+'no'*(feat not in lasso_features_095) for feat in lasso_features]
    rs_lassocox_09_indicator = ['yes'*(feat in lasso_features_09)+'no'*(feat not in lasso_features_09) for feat in lasso_features]
    rs_lassocox_df['in RadiomicScore_LASSO,COX_0.95'] = np.array(rs_lassocox_095_indicator)
    rs_lassocox_df['in RadiomicScore_LASSO,COX_0.9'] = np.array(rs_lassocox_09_indicator)
    rs_lassocox_df.to_csv(os.path.join(saving_dir,'RadiomicScore_LASSO,COX_composition.csv'))
    
    
    ## RadiomicScore_LASSO_i     
    nfold = fold_perf_df.shape[0]
    npat = db.shape[0]
    lasso_scores_array = np.zeros((npat,nfold))
    for i in range(nfold):
        feats = fold_perf_df['selected_features'].iloc[i]
        weights = fold_perf_df['Cox_weights'].iloc[i]
        if type(weights) == str:
            feats = ast.literal_eval(feats)
            weights = ast.literal_eval(weights)
        if im_type == 'MRI':
            feats = adapt_featnames_list([('Flair_')*(name.startswith('original'))+name for name in feats])
        else:
            feats = adapt_featnames_list(feats)
        sum_weights = np.sum( weights )
        aux = np.array( [ db[feats[j]].values * weights[j] for j in range(len(weights)) ])
        lasso_scores_array[:,i] = aux.sum(axis=0)/ sum_weights
    lasso_names = ['$RadiomicScore_{LASSO,'+str(i)+'}$' for i in range(nfold)]
    
    ## df with all scores 
    all_scores_df = pd.DataFrame()
    all_scores_df['PatID'] = db.PatID.values
    all_scores_df['$RadiomicScore_{COX}$'] = rs_cox
    all_scores_df['$RadiomicScore_{COX,0.95}$'] = rs_cox_095
    all_scores_df['$RadiomicScore_{COX,0.9}$'] = rs_cox_09
    all_scores_df['$RadiomicScore_{LASSO,COX}$'] = rs_lassocox
    all_scores_df['$RadiomicScore_{LASSO,COX,0.95}$'] = rs_lassocox_095
    all_scores_df['$RadiomicScore_{LASSO,COX,0.9}$'] = rs_lassocox_09
    all_scores_df[lasso_names] = lasso_scores_array
    
    all_scores_df = all_scores_df.dropna(axis='columns')
    all_scores_df.to_csv(os.path.join(saving_dir,'all_signatures_values.csv'))
    
    plt.close('all')
    return all_scores_df


def plot_Brier(interpolated_time,briers,mean_ibs,std_ibs,saving_dir,name):
    interpolated_time = interpolated_time/12
    tmax = np.max(interpolated_time)
    fig = plt.figure(figsize=(10,10))
    plt.plot([0, tmax], [0.25, 0.25], linestyle='--', lw=2, color='r',
             label='Limit', alpha=.8)

    mean_tpr = np.mean(briers, axis=0)
    plt.plot(interpolated_time, mean_tpr, color='b',
             label=r'Mean Brier curve (IBS = %0.2f $\pm$ %0.2f)' % (mean_ibs, std_ibs),
             lw=2, alpha=.8)
    std_tpr = np.std(briers, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(interpolated_time, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ std. dev.')
    plt.xlabel('Time',fontsize=18)
    plt.ylabel('Brier score',fontsize=18)
    plt.title('Cross-Validation Brier curve',fontsize=18)
    plt.legend(loc="lower right", prop={'size': 15})
    plt.show()
    fig.savefig(os.path.join(saving_dir,'mean_Brier_'+name+'.pdf'))
    return None

def plot_survival_selectedfeatures_boxplots_NEWVERSION(scores,cox_pvs,cox_hrs,aux_df,figname,saving_dir,figsize='small'): #figname : Cox + criterion
    ## scores is a df with features in columns, nb of fold in row, and shows individual coxnet weights
    ## In this new version, only imp_array is modified to take only into account nonzero weights (cases when the features were selected)

    plot_feats = list(aux_df['features'].values)
    plot_feats_labels = plot_feats#adapt_featnames_list(plot_feats)
    
    # Get boxplots of importance weights, Cov px, Cox hr
    imp_array = np.transpose( np.array( [ scores[feat].values[np.nonzero(scores[feat].values)] for feat in plot_feats ]) )
    cox_pvs_array = np.transpose( np.array( [ cox_pvs[feat] for feat in plot_feats ]) )
    cox_hrs_array = np.transpose( np.array( [ cox_hrs[feat] for feat in plot_feats ]) )
    
    
    f, (ax1,ax2,ax3,ax4) = plt.subplots(1,4,sharey=True,figsize=(18,6)*(figsize=='small')+(18,25)*(figsize=='large'))
    plt.yticks(np.arange(1,len(plot_feats)+1),labels=plot_feats_labels)
    plt.ylim([0,len(plot_feats)+1])
    
    ax2.boxplot(imp_array,showmeans=True,meanprops={"marker":"s"},vert=False,labels=list(plot_feats_labels))
    ax2.vlines(0,0,len(plot_feats)+1,'black')
    ax2.set_xlabel('Lasso weigths')

    ax3.boxplot(cox_pvs_array,showmeans=True,meanprops={"marker":"s"},vert=False,labels=list(plot_feats_labels))
    ax3.vlines(0.05,0,len(plot_feats)+1,'black')
    ax3.set_xscale('log')
    ax3.set_xlabel('Cox p-value')
    
    ax4.boxplot(cox_hrs_array,showmeans=True,meanprops={"marker":"s"},vert=False,labels=list(plot_feats_labels))
    ax4.vlines(1,0,len(plot_feats)+1,'black')
    ax4.set_xlabel('Cox hazard ratio ')
    
    ax1.scatter(aux_df['selection_frequency'].values,np.arange(1,len(plot_feats)+1),marker="o")
    #ax4.vlines(1,-1,len(unique_feats),'black')
    ax1.set_xlabel('Percentage of selection')
    
    plt.tight_layout()
    plt.savefig(os.path.join(saving_dir,figname+'_boxplots_nonzero_weights.pdf'))
    return None

def plot_survival_selected_features_correlations(db,features,figname,saving_dir):
    db_corr = db[features]
    #db_corr.columns = adapt_featnames_list(features)
    
    fig = plt.figure()
    plt.axes([0.2,0.2,0.8,0.7])
    ax = fig.gca()
    plot = sns.heatmap(db_corr.corr(method = "spearman"),annot=True,fmt = ".2f",annot_kws={"size":12})
    plot.set_xticklabels( plot.get_xticklabels(), ha='center') 
    plot.set_yticklabels( plot.get_yticklabels() , va='center') 
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16) 
    plt.xticks(rotation=45, ha='right')
    plt.yticks(rotation=0)
    plt.tight_layout()
    plt.show()
    fig.savefig(os.path.join(saving_dir,'correlations_'+figname+'.pdf'),bbox_inches='tight')
    return None

def plot_kaplan_meier_curves(db,survival,final_name_dict,saving_dir,savepv_name=''):
    Feat_KM = [name for name in db.columns.values if name != 'PatID']
    T = survival['PFS'] # duration
    C = survival["RECURRENCE"] # censorship : 1 if recurrence, 0 if censored
    logrank_pv_array = np.zeros(len(Feat_KM))
    
    for j,feat in enumerate(Feat_KM):
        targ = db.loc[:,feat]
        # Logrank test
        results = logrank_test(T.loc[(targ == 0)],T.loc[(targ == 1)],C.loc[(targ == 0)],C.loc[(targ == 1)])
        pv = results.p_value
        logrank_pv_array[j] = pv
        
        if pv<0.05:
            ## Figure without Pet status
            kmf = KaplanMeierFitter()
            palette = ["windows blue","tomato"]
            plt_palette = ["xkcd:"+name for name in palette]
            sns.set_palette(sns.xkcd_palette(palette))
            ##SET UP PLOT
            f = plt.figure(figsize=(10,6))
            ax = f.add_subplot(111)
            if pv < 0.001:
                ax.text(0.05,.05,'$p<0.001$',fontsize=18)
            elif pv<0.050 and round(pv,3)==0.050:
                ax.text(0.05,.05,'$p=%0.4f$' % pv,fontsize=18)
            else:
                ax.text(0.05,.05,'$p=%0.3f$' % pv,fontsize=18)
            if feat not in final_name_dict.keys():
                plt.title(r''+ feat) 
            else:
                plt.title(r''+ final_name_dict[feat]+'')            
            plt.xlabel('Time (years)')
            plt.ylabel('Recurrence probability')
            sns.set_context("talk")
            d={} #to store the models
            #vlines = []
            i=0
            max_surv=0
            
            ## Curve of Feature
            #loop through segmentation variable, plot on same axes
            for segment in sorted(targ.unique()): 
                ix = (targ == segment)
                d['kmf{}'.format(i+1)] = kmf.fit(T.loc[ix],C.loc[ix], label=segment)
                ax = kmf.plot(ax=ax, show_censors=True, ci_alpha=0.1, censor_styles={'ms': 6, 'marker': 's'})
                ax.set_xlim([T.min(),T.max()])
                ax.set_ylim([0,1])
                
                # Median survival 
                med_surv_time = np.amin([kmf.median_survival_time_,np.amax(T.values)])
                plt.vlines(med_surv_time,0,0.5,[plt_palette[i]],'dashed')
                max_surv = np.amax([max_surv,med_surv_time])
                i+=1
            
            plt.hlines(0.5,0,max_surv,["grey"],'dashed')

            ##LEGEND
            if feat in final_name_dict.keys():
                patches = [ 
                            mpatches.Patch(color="xkcd:windows blue", label= r' Smaller score'),
                            mpatches.Patch(color="xkcd:tomato", label= r' Larger score')
                          ]
            else:
                patches = [ 
                            mpatches.Patch(color="xkcd:windows blue", label= r' Smaller score'),
                            mpatches.Patch(color="xkcd:tomato", label= r' Larger score')
                          ]
    
            plt.legend(handles=[patches[0],patches[1]],bbox_to_anchor=(0, 1, 0, -1.2),loc='upper left')
            plt.xlabel('Time (years)')
            plt.tight_layout()
            f.savefig(os.path.join(saving_dir,'Final_KM_'+feat+'.pdf'))
        
    if len(savepv_name) >0 :
        final_db = pd.DataFrame()
        final_db['Scores'] = Feat_KM
        final_db['Logrank_pv'] = logrank_pv_array
        final_db = final_db.sort_values(by='Logrank_pv', ascending=True)
        final_db.to_csv(os.path.join(saving_dir,savepv_name+'.csv'))
    return None


def get_correlations_of_best_signatures_after_psm(best_signatures,best_signatures_05,psm_data,im_type,savingdir):
    best_signatures_scores_df = psm_data[['PatID']+['$'+name+'$' for name in best_signatures]]
    best_signatures_scores_df.columns = ['PatID']+[r''+(im_type+'-based ')*('Radiomic' in name)+'$'+name+'$' for name in best_signatures]
    best_signatures_scores_df.to_csv(savingdir+'/best_signatures.csv')

    ## Correlations
    signatures_correlations = best_signatures_scores_df.corr(method = "spearman")
    plt.figure(figsize=(13,9))
    plot = sns.heatmap(signatures_correlations,annot=True,fmt = ".2f",annot_kws={"size":12})
    plot.set_xticklabels( plot.get_xticklabels(), ha='center')
    plot.set_yticklabels( plot.get_yticklabels() , va='center')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig(savingdir+'/correlations_of_best_signatures.pdf',bbox_inches='tight')

    best_signatures_scores_df_05 = psm_data[['PatID']+['$'+name+'$' for name in best_signatures_05]]
    best_signatures_scores_df_05.columns = ['PatID']+[r''+(im_type+'-based ')*('Radiomic' in name)+'$'+name+'$' for name in best_signatures_05]
    signatures_correlations_05 = best_signatures_scores_df_05.corr(method = "spearman")
    
    plt.figure(figsize=(13,9))
    plot = sns.heatmap(signatures_correlations_05,annot=True,fmt = ".2f",annot_kws={"size":12})
    plot.set_xticklabels( plot.get_xticklabels(), ha='center')
    plot.set_yticklabels( plot.get_yticklabels() , va='center')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig(savingdir+'/correlations_of_best_signatures_05.pdf',bbox_inches='tight')
    plt.figure(figsize=(18,10))
    plot = sns.heatmap(signatures_correlations_05,annot=True,fmt = ".2f",annot_kws={"size":12})
    plot.set_xticklabels( plot.get_xticklabels(), ha='center')
    plot.set_yticklabels( plot.get_yticklabels() , va='center')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig(os.path.join(savingdir,'correlations_of_best_signatures_big.pdf'),bbox_inches='tight')
    return None


def get_composition_of_best_signatures_after_psm_and_individual_psm(best_signatures,fold_perf_df,psm_data,target,im_type,compositiondir,savingdir):
    ## Get table of relevant signatures' compositions for validation
    sign_namelist = []
    sign_features = []
    sign_weights = []
    for name in best_signatures:
        if ('LASSO' in name) & ('COX' in name):
            lassocox_compo = pd.read_csv(os.path.join(compositiondir,'RadiomicScore_LASSO,COX_composition.csv'),index_col=0)
            if '0.95' in name:
                feats = list(lassocox_compo['features'][lassocox_compo['in RadiomicScore_LASSO,COX_0.95'] == 'yes'].values)
                weights = list(lassocox_compo['Cox_weights'][lassocox_compo['in RadiomicScore_LASSO,COX_0.95'] == 'yes'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{LASSO,COX,0.95}']*nb
                sign_features += feats
                sign_weights += weights
            elif '0.9' in name:
                feats = list(lassocox_compo['features'][lassocox_compo['in RadiomicScore_LASSO,COX_0.9'] == 'yes'].values)
                weights = list(lassocox_compo['Cox_weights'][lassocox_compo['in RadiomicScore_LASSO,COX_0.9'] == 'yes'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{LASSO,COX,0.9}']*nb
                sign_features += feats
                sign_weights += weights
            else:
                feats = list(lassocox_compo['features'].values)
                weights = list(lassocox_compo['Cox_weights'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{LASSO,COX}']*nb
                sign_features += feats
                sign_weights += weights
        elif 'COX' in name:
            cox_compo = pd.read_csv(os.path.join(compositiondir,'/RadiomicScore_COX_composition.csv'),index_col=0)
            if '0.95' in name:
                feats = list(cox_compo['features'][cox_compo['in RadiomicScore_COX_0.95'] == 'yes'].values)
                weights = list(cox_compo['Cox_weights'][cox_compo['in RadiomicScore_COX_0.95'] == 'yes'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{COX,0.95}']*nb
                sign_features += feats
                sign_weights += weights
            elif '0.9' in name:
                feats = list(cox_compo['features'][cox_compo['in RadiomicScore_COX_0.9'] == 'yes'].values)
                weights = list(cox_compo['Cox_weights'][cox_compo['in RadiomicScore_COX_0.9'] == 'yes'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{COX,0.9}']*nb
                sign_features += feats
                sign_weights += weights
            else:
                feats = list(cox_compo['features'].values)
                weights = list(cox_compo['Cox_weights'].values)
                weights = [round(float(w),3) for w in weights]
                nb = len(feats)
                sign_namelist += ['RadiomicScore_{COX}']*nb
                sign_features += feats
                sign_weights += weights
        elif 'Radiomic' in name:
            fold_nb = int(name.split('LASSO,')[1].split('}')[0])
            
            feats = fold_perf_df['selected_features'].iloc[fold_nb]
            weights = fold_perf_df['Cox_weights'].iloc[fold_nb]
            if type(weights) == str:
                feats = ast.literal_eval(feats)
                weights = ast.literal_eval(weights)
            if im_type == 'MRI':
                feats = adapt_featnames_list([('Flair_')*(name.startswith('original'))+name for name in feats])
            else:
                feats = adapt_featnames_list(feats)
            weights = [round(float(w),3) for w in weights]
            
            nb = len(feats)
            sign_namelist += ['RadiomicScore_{LASSO,'+str(fold_nb)+'}']*nb
            sign_features += feats
            sign_weights += weights
    best_compo_df = pd.DataFrame()
    best_compo_df['Signature name'] = sign_namelist
    best_compo_df['Features'] = [f.replace('$','') for f in sign_features]
    best_compo_df['Weights'] = sign_weights
    best_compo_df.to_csv(os.path.join(savingdir,'best_signatures_composition.csv'))
    
    # Perform PSM on individual features involved in best signatures
    out_single = os.path.join(savingdir,'single')
    if os.path.isdir(out_single) == False:
        os.mkdir(out_single)
        
    # Get target for individual feat to check 
    radiomic_signatures_survival_groups_df = pd.DataFrame()
    for feat in list(np.unique(sign_features)):
        med = psm_data[feat].median()
        radiomic_signatures_survival_groups_df.loc[:,feat] = 1*(psm_data.loc[:,feat]>med)
    radiomic_signatures_survival_groups_df['PatID'] = psm_data['PatID'].values

    # set the figure shape and size of outputs
    sns.set(rc={'figure.figsize':(10,8)}, font_scale = 1.3)
    covar = ['EOR','Age','ADJUVANT Rx','Sex','PET','Histology','Grade'] * (im_type=='MRI') + ['EOR','Age','ADJUVANT Rx','Sex','Histology','Grade'] * ('PET' in im_type)
    utils_propensity_score_matching.PropScoreMatching(radiomic_signatures_survival_groups_df,target,im_type,covar,20,out_single)
    return None


def get_cohort_repartition_before_psm_from_signature(signature_name,target,im_type,compositiondir):
    groups = pd.read_csv(os.path.join(compositiondir,'/survival_groups.csv'),index_col=0)
    low_pat = groups['PatID'].loc[groups[signature_name]==0]
    large_pat = groups['PatID'].loc[groups[signature_name]==1]
    
    data = pd.read_excel(os.path.join("databases","Clinical_database.xlsx"),sheet_name=0)
    data["EOR"] = data["EOR"].apply(lambda x: 0*(x=='Total')+1*(x=='Subtotal')+2*(x=='Partial')+3*(x=='Biopsy'))
    data['ADJUVANT Rx'] = data['ADJUVANT Rx'].astype(int)
    data['Sex'] = data['Sex'].astype(int)
    #data['RECURRENCE'] = data['RECURRENCE'].astype(int)
    data['Grade'] = data['Grade'].astype(int)
    data["PET"] = data["PET"].apply(lambda x: 0*(x=='neg')+1*(x=='pos'))
    data["Histology"] = data["Histology"].apply(lambda x: 0*(x=='Astrocytoma')+1*(x=='Oligodendroglioma'))
    
    low_data = data.loc[data['PatID'].isin(low_pat)]
    large_data = data.loc[data['PatID'].isin(large_pat)]
    
    if low_data.shape[0] < len(low_pat):
        low_pat = list(low_data['PatID'].values)
    if large_data.shape[0] < len(large_pat):
        large_pat = list(large_data['PatID'].values)
    
    scores = pd.read_csv(os.path.join(compositiondir,'best_signatures.csv'),index_col=0)
    try:
        low_scores = scores[im_type+'-based '+signature_name].loc[scores['PatID'].isin(low_pat)]
        large_scores = scores[im_type+'-based '+signature_name].loc[scores['PatID'].isin(large_pat)]
    except:
        low_scores = scores[im_type+'-based '+signature_name[1:-1]].loc[scores['PatID'].isin(low_pat)]
        large_scores = scores[im_type+'-based '+signature_name[1:-1]].loc[scores['PatID'].isin(large_pat)]
    
    low_target = target.loc[target['PatID'].isin(low_pat)]
    large_target = target.loc[target['PatID'].isin(large_pat)]
    
    print('nb of pat : ' , str(len(low_pat)),str(len(large_pat)))
    print('scores medians : ' , str(np.median(low_scores)),str(np.median(large_scores)))
    print('nb astro : ' , str(np.sum(low_data["Histology"]==0)),str(np.sum(large_data["Histology"]==0)))
    print('nb oligo : ' , str(np.sum(low_data["Histology"]==1)),str(np.sum(large_data["Histology"]==1)))
    print('nb PET + : ' , str(np.sum(low_data["PET"]==1)),str(np.sum(large_data["PET"]==1)))
    print('nb adjuvant + : ' , str(np.sum(low_data["ADJUVANT Rx"]==1)),str(np.sum(large_data["ADJUVANT Rx"]==1)))
    print('nb EOR total : ' , str(np.sum(low_data["EOR"]==0)),str(np.sum(large_data["EOR"]==0)))
    print('nb EOR subtotal : ' , str(np.sum(low_data["EOR"]==1)),str(np.sum(large_data["EOR"]==1)))
    print('nb EOR partial : ' , str(np.sum(low_data["EOR"]==2)),str(np.sum(large_data["EOR"]==2)))
    print('nb EOR biopsy : ' , str(np.sum(low_data["EOR"]==3)),str(np.sum(large_data["EOR"]==3)))
    print('nb recurrence : ' , str(np.sum(low_target["RECURRENCE"]==1)),str(np.sum(large_target["RECURRENCE"]==1)))
    print('Median PFS : ' , str(np.median(low_target["PFS"])),str(np.median(large_target["PFS"])))
    print('nb grade 2: ' , str(np.sum(low_data["Grade"]==2)),str(np.sum(large_data["Grade"]==2)))
    print('nb grade 3 : ' , str(np.sum(low_data["Grade"]==3)),str(np.sum(large_data["Grade"]==3)))
    print('nb grade 4 : ' , str(np.sum(low_data["Grade"]==4)),str(np.sum(large_data["Grade"]==4)))
    return None



