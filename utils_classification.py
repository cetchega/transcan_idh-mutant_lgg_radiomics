#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 22 23:12:34 2023

@author: cetchega
"""
##### IMPORTS #####
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns
from matplotlib.transforms import ScaledTranslation
from matplotlib.gridspec import GridSpec

import scipy.stats as stats
from statsmodels.api import Logit

from sklearn.model_selection import RepeatedStratifiedKFold, StratifiedKFold, GridSearchCV
from groupyr import LogisticSGL
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import roc_curve,roc_auc_score,accuracy_score,confusion_matrix, brier_score_loss
from sklearn.calibration import calibration_curve

from utils_general import adapt_featnames_list, adapt_featname

##### FUNCTIONS #####
def univariate_mannwhitney_bonferroni(dataset,target,outcomedict,savingdir):
    db_mann = pd.DataFrame(columns=['feature', 'pvalue', 'corrected_pvalue']) # dataframe for features and associated pvalues
    featnames = dataset.columns.drop('PatID')
    nfeat = len(featnames)
    dataset0 = dataset.loc[target==0]
    dataset1 = dataset.loc[target==1]
    for feature in featnames:
        # Split data according to target values, apply the test and store the result
        group1 = dataset0[feature]
        group2 = dataset1[feature]
        st,pv = stats.mannwhitneyu(group1, group2)
        db_mann = db_mann.append({'feature':feature,'pvalue':pv, 'corrected_pvalue':pv*nfeat},ignore_index=True)
    db_mann = db_mann.sort_values(by=['pvalue'],axis=0).reset_index(drop=True)
    db_mann.to_csv(savingdir + '/MannWhitney_pvalues.csv')
    
    print('----- Mann-Whitney test with Bonferroni correction -----')
    print('Significatively different features:')
    print(db_mann[db_mann['corrected_pvalue']<0.05])
    mw_corrected_feats = db_mann['feature'][db_mann['corrected_pvalue']<0.05].values
    #mw_original_feats = db_mann['feature'][db_mann['pvalue']<0.05].values
    
    if len(mw_corrected_feats) != 0:
        for feat in mw_corrected_feats:
            plot_univariate_boxplot(dataset,feat,target,outcomedict,savingdir)
        
        corr_matrix = dataset[mw_corrected_feats].corr()
        featnames_plot = np.array(adapt_featnames_list(mw_corrected_feats),dtype=str)
        
        fig = plt.figure(figsize=(24,22))
        plt.axes([0.2,0.2,0.8,0.7])
        ax = fig.gca()
        plot = sns.heatmap(corr_matrix,ax=ax,yticklabels=featnames_plot,xticklabels=featnames_plot,annot=True,annot_kws={"size":12})
        plot.set_xticklabels(plot.get_xticklabels(), ha='center')
        plot.set_yticklabels(plot.get_yticklabels(), va='center')
        ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
        plt.yticks(rotation=0)
        plt.show()
        fig.savefig(savingdir+'/MannWhitney_FeaturesCorrelations.pdf')
    return None

def plot_univariate_boxplot(df,feat_name,target,outcomedict,savingdir):
    toplot = []
    names = []
    for key in target.unique():
        toplot.append( df[feat_name].iloc[ target[target==key].index.tolist() ].values )
        names.append(outcomedict[key])
    fig = plt.figure(figsize=(10,6))
    ax = plt.gca()
    ax.set_yscale('log')
    plt.boxplot(toplot)
    plt.title(r" Boxplot of $"+adapt_featname(feat_name)+'$')
    plt.xticks(np.arange(1,len(toplot)+1),names,fontsize=12)
    plt.show()
    fig.savefig(savingdir+'/'+ feat_name + "_boxplot.pdf")
    return None

def reducer_MannWhitney(dataset,target):
    nfeat = dataset.shape[1]
    ind0 = np.where(target == 0)[0]
    ind1 = np.where(target == 1)[0]
    dataset0 = dataset[ind0,:]
    dataset1 = dataset[ind1,:]
    
    scores = np.zeros(nfeat)
    pval = np.zeros(nfeat)
    for i in range(nfeat):
        # Split data according to target values, apply the test and store the result
        group1 = dataset0[:,i]
        group2 = dataset1[:,i]
        st,pv = stats.mannwhitneyu(group1, group2)
        scores[i] = 1-pv #On définit un score d'importance
        pval[i] = pv
        i+=1
    return scores, pval

def reducer_LogisticRegression(dataset,target):
    nfeat = dataset.shape[1]
    scores = np.zeros(nfeat)
    pval = np.zeros(nfeat)
    for i in range(nfeat):
        model = Logit(target, dataset[:,i])
        model_fit = model.fit()
        pv = model_fit.pvalues

        scores[i] = 1-pv #On définit un score d'importance
        pval[i] = pv
        i+=1
    return scores, pval

def attribute_groups_to_features(featnames,genecols):
    """
    Build a list of same length as featnames for their feature groups.

    Parameters
    ----------
    featnames : list
        Name of features.
    genecols : list
        Name of genes.

    Returns
    -------
    featgroups : list
        Feature group for each feature.

    """
    SUV_list = ['SUVmax','SUVmean','SUVnorm']
    alter_list = ['MTV','MTB']
    hi_list = ['HI','WHI','SKEW','KURT']

    featgroups = []
    for name in featnames:
        if name in SUV_list:
            featgroups.append("SUV")
        elif name in alter_list:
            featgroups.append("Metabolic")
        elif name in hi_list:
            featgroups.append("HI")
        elif name == 'SUVR':
            featgroups.append(name)
        elif name.startswith('Pet_'): # Cas où on mélange Pet et flair
            featgroups.append(name[13: name.index('_',13) ])
        elif name.startswith('Flair_'): # Cas où on mélange Pet et flair
            featgroups.append(name[15: name.index('_',15) ])
        elif name in genecols:
            featgroups.append('Genes')
        else: # Other Radiomics
            featgroups.append(name[9: name.index('_',9) ])
    return featgroups

# Get list of arrays where each array shows feat indexes in the group
def get_indices_of_GroupedFeatures(listoffeaturesgroups):
    """
    Parameters
    ----------
    listoffeaturesgroups : list of len nb_features
        Group for each feature.

    Returns
    -------
    final_list : list of arrays
        Each array is for a group and stores the indices of corresponding features.

    """
    final_list = []
    copy_of_LOG = np.copy(listoffeaturesgroups)
    while len(copy_of_LOG) > 0:
        sample = copy_of_LOG[0] # Get a group name
        sample_indices = list(filter(lambda x: listoffeaturesgroups[x] == sample, range(len(listoffeaturesgroups))))
        final_list.append( np.array(sample_indices) )
        copy_of_LOG = copy_of_LOG[copy_of_LOG!=sample]
    return final_list

def write_pipeline(pipelist,algos,savingdir):
    #open file
    file = open(savingdir+"/Pipeline(s).txt", "w")

    # Write algo as used or not, to have all parameters if needed
    file.write("Algos\n\n")
    for key in algos.keys():
        file.write(key+" \n" + repr(algos[key]) + "\n\n")
    file.write("Pipeline(s)\n\n")
    for element in pipelist:
        #convert variable to string
        pipeobj = repr(element)
        file.write(pipeobj + "\n\n")
    #close file
    file.close()
    return None


def cv_remove_correlated_features(xtrain,xtest,threshold=0.95):
    corr_matrix = xtrain.corr(method = "spearman").abs()
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),k=1).astype(bool))
    to_drop = [column for column in upper.columns if any(upper[column]>0.95)]
    to_keep = [col for col in upper.columns if col not in to_drop]
    return xtrain[to_keep], xtest[to_keep]

def cv_store_confusionmatrix_scores(j,test_truth,test_predict,sensitivity_array,specificity_array,ppv_array,npv_array):
    try:
        cm = confusion_matrix(test_truth,test_predict)
        tn, fp, fn, tp = cm.ravel()
        sensitivity_array[j] = tp/(tp+fn)
        specificity_array[j] = tn/(fp+tn)
        if fp+tp == 0:
            ppv_array[j] = np.nan
        else:
            ppv_array[j] = tp/(tp+fp)
        if tn+fn==0:
            npv_array[j] = np.nan
        else:
            npv_array[j] = tn/(tn+fn)
    except:
        pass
    return sensitivity_array,specificity_array,ppv_array,npv_array

def cv_store_auc_roc(j,pipe,xtest,ytest,auc_array,brier_array,calibration_array,calibration_indices,tprs_array,mean_fpr,outer_nb,n_repeat):
    try:
        pred_prob=pipe.predict_proba(xtest) 
        auc_array[j] = roc_auc_score(ytest,pred_prob[:,1]) 
        fpr, tpr, thresholds = roc_curve(ytest, pred_prob[:, 1])
        brier_array[j] = brier_score_loss(ytest,pred_prob[:,1])
        k0, k1 = calibration_indices
        calibration_array[1,k0:k1] = pred_prob[:,1]
        # FAux calibration_array[1,j*outer_nb*n_repeat:(j+1)*outer_nb*n_repeat] = pred_prob[:,1]
    except:
        auc_array[j] = roc_auc_score(ytest,pipe.decision_function(xtest)) 
        fpr, tpr, thresholds = roc_curve(ytest, pipe.decision_function(xtest))
    tprs_array.append(np.interp(mean_fpr, fpr, tpr))  
    tprs_array[-1][0] = 0.0
    return auc_array, brier_array, calibration_array, tprs_array

def cv_count_misclassifications(target,test_index,test_predictions,Nb_error):
    test_score=[(target.iloc[test_index[k]] == test_predictions[k]) for k in range(len(test_predictions))]
    for l in range(len(test_score)):
        if test_score[l] == False:
            Nb_error[0,test_index[l]]+=1
    return Nb_error

def cv_plot_calibration_figure(calibration_data,savingdir):
    prob_true, prob_pred = calibration_curve(calibration_data[0,:], calibration_data[1,:], n_bins=5)
    
    fig = plt.figure() 
    gs = GridSpec(2, 1)
    
    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot([0,1],[0,1],':',c='black',label='fold_perf_dfectly calibrated')
    ax0.plot(prob_pred,prob_true,'-.',label='Classifier')
    ax0.set_xlabel('Mean predicted probability')
    ax0.set_ylabel('Fraction of positives')
    ax0.grid()
    ax0.set_title("Calibration plot")
    
    # Add histogram
    ax1 = fig.add_subplot(gs[1, 0])
    ax1.hist(
        calibration_data[1,:],
        range=(0, 1),
        bins=10
    )
    ax1.set(title='Predicted probabilities', xlabel="Mean predicted probability", ylabel="Count")
    plt.tight_layout()
    plt.show()
    fig.savefig(savingdir+'/calibration_plot.png')
    return None

def cv_save_classif_results(scores_namelist,scores_list,misclassif_array,features_votes,features_scores,patients,features,savingdir):
    # Scores 
    scores_mean, scores_std = [], []
    with open(savingdir+'/readme.txt', 'a') as f:
        f.write('\n')
        f.write('Clasification scores\n')
        print('----- Classification scores -----')
        for i, score in enumerate(scores_list):
            scores_mean += [np.nanmean(score)]
            scores_std += [np.std(score)]
            
            summary = scores_namelist[i]+': {:.2} +/- {:.2}\n'.format(np.nanmean(score),np.std(score))
            f.write(summary)
            print(summary)
            
    scores_array =  np.array( [ "%.2f +/- %.2f" %(scores_mean[j],scores_std[j]) for j in range(len(scores_namelist)) ]   ).reshape((1,len(scores_namelist)))
    Scores_df=pd.DataFrame(scores_array,columns=scores_namelist)
    Scores_df.to_csv(savingdir+'/classification_scores.csv', encoding='utf-8')

    # Misclassified patients
    misclassif_df = pd.DataFrame(misclassif_array,columns=patients)
    misclassif_df.to_csv(savingdir+'/misclassif.csv', encoding='utf-8')
    
    # Features importance : selection rate and scores
    # First row : normalized votes
    # Then : normalized scores
    nfolds = float(features_scores.shape[0])
    featimp_df = pd.DataFrame(features_votes*100/nfolds, columns = features,dtype=object)
    max_score = float(np.max(features_scores.ravel()))
    aux_df = pd.DataFrame(features_scores/max_score, columns = features)
    featimp_df = featimp_df.append(aux_df,ignore_index=True)
    featimp_df = featimp_df.sort_values(0,axis=1,ascending=False) # On range selon le % de votes
    featimp_df.to_csv(savingdir+'/features_importances.csv',encoding='utf-8')
    return None

def get_pipeline_featurescores(pipeline_reducer):
    try: 
        Feature_Scores = pipeline_reducer.estimator_.feature_importances_
    except:
        try:
            Feature_Scores = pipeline_reducer.estimator_.scores_
        except:
            try:
                Feature_Scores = pipeline_reducer.scores_
            except:
                try:
                    Feature_Scores = pipeline_reducer.estimator_.coef_
                except:
                    Feature_Scores = None
    return Feature_Scores

def nested_cv(pipes,search_space,db,featnames,pat,target,genecols,savingdir,inner_nb=5,outer_nb=5,n_repeat=100):
    print('----- Nested Stratified {}-inner, {}-outer fold cross-validation with {} repetitions. -----'.format(inner_nb,outer_nb,n_repeat))
    X = db[featnames]
    
    inner_kf = StratifiedKFold(n_splits=inner_nb,shuffle=True)
    outer_kf = RepeatedStratifiedKFold(n_splits=outer_nb,n_repeats=n_repeat)

    ## For scores
    inner_loop_won_params = []
    inner_loop_scores = np.zeros(outer_nb*n_repeat)
    
    score_names = ['Inner_Score','Accuracy','AUC','Sensitivity','Specificity','PPV','NPV','Brier']
    outer_accuracy = np.zeros(outer_nb*n_repeat)
    outer_auc=np.zeros(outer_nb*n_repeat)
    outer_sensitivity=np.zeros(outer_nb*n_repeat)
    outer_specificity=np.zeros(outer_nb*n_repeat)
    outer_ppv=np.zeros(outer_nb*n_repeat)
    outer_npv=np.zeros(outer_nb*n_repeat)
    outer_brier=np.zeros(outer_nb*n_repeat)
    
    # True labels and predicted probas in test sets
    calibration_data = np.zeros((2,n_repeat*len(target)))

    # Rocs for each winner and average
    tprs = []
    mean_fpr = np.linspace(0,1,100)
    
    # Get nb of misclassified patients in test sets
    pat_error=np.zeros((1,len(target)))
    
    # Get feature votes, scores, and number of selected features in each fold
    feat_votes = np.zeros((1,len(featnames)))
    feat_scores = np.zeros((outer_nb*n_repeat,len(featnames))) # Contains scores for each feature and for each fold
    
    nb_SelectedFeat = np.zeros(outer_nb*n_repeat)

    j=0 # To count nb of outer cv loop
    k0=0 # To track calibration array 
    # Looping through the outer loop, feeding each training set into a GSCV as the inner loop
    for train_index,test_index in outer_kf.split(X,target):
        print(str(j)+'/'+str(outer_nb*n_repeat))
        Xtrain = X.loc[train_index]
        ytrain = target.loc[train_index]
        Xtest = X.loc[test_index]
        ytest = target.loc[test_index]
        
        k1 = k0+len(ytest.values)
        calibration_data[0,k0:k1] = ytest.values
        
        ## Remove correlated features         
        Xtrain, Xtest = cv_remove_correlated_features(Xtrain,Xtest)
        # If sparse group lasso, has to be redefined
        try :
            print(pipes['reducer'].available_models['sparse_group_lasso'])
        except:
            pass
        else:
            group_indices = get_indices_of_GroupedFeatures(attribute_groups_to_features(Xtrain.columns.values,genecols))
            sgl_model = LogisticSGL(l1_ratio=0.6, alpha=1, groups=group_indices, scale_l2_by='group_length', fit_intercept=True, max_iter=10000, tol=1e-07, warm_start=False, verbose=1, suppress_solver_warnings=False, include_solver_trace=False)
            pipes['reducer'].available_models['sparse_group_lasso'] = SelectFromModel(sgl_model)
        
        ## Perform GridSearch
        GSCV = GridSearchCV(estimator=pipes,param_grid=search_space,cv=inner_kf,scoring='balanced_accuracy')
        # GSCV is looping through the training data to find the best parameters. This is the inner loop
        GSCV.fit(Xtrain,ytrain.values.ravel())
        
        # Appending the "winning" hyper parameters and their associated score
        inner_loop_won_params.append(GSCV.best_params_)
        inner_loop_scores[j] = GSCV.best_score_
    
        # The best hyper model from GSCV is now being tested on the unseen outer loop test data.
        pred = GSCV.predict(Xtest)
        outer_accuracy[j] = accuracy_score(ytest,pred)
        # Confusion matrix : sensitivity, specificity, ppv, npv
        outer_sensitivity,outer_specificity,outer_ppv,outer_npv = cv_store_confusionmatrix_scores(j,ytest,pred,outer_sensitivity,outer_specificity,outer_ppv,outer_npv)
        # Predict proba, AUC & ROC curves
        outer_auc, outer_brier, calibration_data, tprs = cv_store_auc_roc(j,GSCV,Xtest,ytest,outer_auc,outer_brier,calibration_data,[k0,k1],tprs,mean_fpr,outer_nb,n_repeat)
        k0=k1
        # Misclassified patients 
        pat_error = cv_count_misclassifications(target,test_index,pred,pat_error)
        ## Features importance : mask of selected features, votes, nb, scores
        selected_features_mask = GSCV.best_estimator_.named_steps['reducer'].selected_model.get_support(indices=False)
        selected_features_indices = [featnames.index(selected_feature) for selected_feature in Xtrain.columns.values[selected_features_mask]]
        feat_votes[0,selected_features_indices] += 1
        # Nb of selected features
        nb_SelectedFeat[j] = np.sum(selected_features_mask)
        # Importance score of each features 
        feature_scores = get_pipeline_featurescores(GSCV.best_estimator_.named_steps['reducer'].selected_model)
        ## If RFE, len(feature_scores) < len(Xtrain) since we get only the scores of selected features
        featarray = Xtrain.columns.values        
        if len(feature_scores) < len(Xtrain.columns.values): # RFE
            featarray = Xtrain.columns.values
            scoreFeatureIndices = [featnames.index(i) for i in featarray[GSCV.best_estimator_.named_steps['reducer'].selected_model.get_support()]]
        else:
            scoreFeatureIndices = [featnames.index(i) for i in Xtrain.columns.values]
        
        try:
            feature_scores.all() == None
        except:
            feat_scores[j,:] = np.empty((1, len(featnames))) * np.nan
        else:
            feat_scores[j,scoreFeatureIndices] = feature_scores
                
        j+=1
    # End of for loop
        
    ## Calibration plot
    cv_plot_calibration_figure(calibration_data,savingdir)
    
    ## Results
    scores_listofarrays = [inner_loop_scores, outer_accuracy, outer_auc, outer_sensitivity, outer_specificity, outer_ppv, outer_npv, outer_brier]
    cv_save_classif_results(score_names,scores_listofarrays,pat_error,feat_votes,feat_scores,pat,featnames,savingdir)
    
    # Dictionary of winning pipelines : change keys to drop "__selected_model"
    winning_pipelines = []
    for winning_dict in inner_loop_won_params:
        new_keys = [ name.replace("__selected_model", "") for name in winning_dict.keys()]
        new_dict = dict(zip(new_keys, list(winning_dict.values())))
        winning_pipelines+=[new_dict]
    
    ## Winning pipelines (allows to have nb of selected features by pipeline)
    win_pipes_df = pd.DataFrame()
    win_pipes_df['Pipelines'] = list(map(dict, (tuple(sub.items()) for sub in inner_loop_won_params)))
    win_pipes_df['InnerScores'] = inner_loop_scores
    win_pipes_df['Nfeats'] = nb_SelectedFeat
    win_pipes_df['Pipenames'] = winning_pipelines
    win_pipes_df.to_csv(savingdir+'/selected_pipelines.csv', encoding='utf-8')
    
    return tprs

def barplot_best(df,row_index,title,outcome_name,savingdir,bar_labels=""):
    names = df.columns.values
    row = df.iloc[row_index].values
    
    # When mean +/- std
    mean_list = [ float(item.split()[0]) for item in row ]
    std_list = [ float(item.split()[2]) for item in row ]

    fig = plt.figure(figsize=(12,8))
    plt.axes([0.15,0.2,0.7,0.7])
    ax = fig.gca()
    barplot=plt.bar(range(len(names)), np.array(mean_list), align='center',yerr=np.array(std_list))
    plt.xticks(range(len(names)), names, rotation=90)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 18)
    #plt.tight_layout()

    if len(bar_labels) != 0:
        barplot_autolabel(ax,barplot,bar_labels)
        ax.set_title("Mean nb of selected features and Nb of wins")
        if outcome_name == 'survival':
            pipenames = names
        else:
            try:
                pipenames = adapt_nested_pipenames(names)
            except:
                pipenames = names
        ax.set_xticklabels(pipenames)
    else:
        ax.set_ylim(top=1)
    plt.show()
    fig.savefig(savingdir+'/best_'+title+'.pdf')
    return None

def barplot_autolabel(ax,bar_plot,bar_label):
    for idx,rect in enumerate(bar_plot):
        height = rect.get_height()
        ax.text(0.1*height, rect.get_y() + rect.get_height()/2,# + rect.get_width()/2.,
                bar_label[idx],
                ha='left', va='center', rotation=0)
    return None

def adapt_nested_pipenames(list_pipenames): #Nfeat_byPipe_df.columns[1:]
    newlist = []
    for pipe in list_pipenames:
        if pipe == 'All':
            newname = pipe
        else:
            # Take dict, remove __selected_model
            new_keys = [ name.replace("__selected_model", "") for name in pipe.keys()]
            new_dict = dict(zip(new_keys, list(pipe.values())))
            newname = 'Scaler : ' + str(new_dict['scaler']) + ' ' + 'Reducer : ' + str(new_dict['reducer']) + ' ' + 'Classifier : ' + str(new_dict['classifier'])
        newlist += [newname]
    return newlist

def plot_heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    #cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    #cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_yticklabels(row_labels)
    ax.set_xticklabels(col_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im#, cbar

def annotate_heatmap(im, data=None,bis=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt([data[i, j],bis[i,j]], None), **kw)
            texts.append(text)

    return texts

def heatmap_misclassified(pat,target,pat_score,outcome_dict,savingdir,pipes=["All"]):
    ind0 = target[target==0].index
    ind1 = target[target==1].index
    pat0 = pat[ind0]
    pat1 = pat[ind1]
    pat_score0 = pat_score[pat0]
    pat_score1 = pat_score[pat1]

    x0 = pat0
    x1 = pat1

    if pipes != ["All"]:
        y=['pipe '+ str(i+1) for i in range(len(pipes))] ## Methods to compare
    else:
        y=pipes

    # Mean and Std of Pat errors: arrays of size (N_methods * N_pat)
    zmean0 = pat_score0
    zmean1 = pat_score1

    # Plot
    fig, (ax1, ax2) = plt.subplots(2,1, figsize=(30, 8))
    ax1.set_title(outcome_dict[0],fontsize=18, fontweight='bold')
    im0 = plot_heatmap(zmean0, y, x0, ax=ax1, cmap='coolwarm')
    annotate_heatmap(im0,bis=zmean0.values, valfmt="{x[0]:.1f}") # To write score inside the tiles

    ax2.set_title(outcome_dict[1],fontsize=18, fontweight='bold')
    im1 = plot_heatmap(zmean1, y, x1, ax=ax2, cmap='coolwarm')
    annotate_heatmap(im1,bis=zmean1.values, valfmt="{x[0]:.1f}") # To write score inside the tiles

    plt.show()
    fig.savefig(savingdir+'/misclassified_patients.pdf')
    return None



def plot_bestpipe_ROC(mean_fpr,tprs_best,auc_string,savingdir):
    mean_auc = float(auc_string.split()[0])
    std_auc = float(auc_string.split()[2])

    fig = plt.figure(figsize=(10,10))
    plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
             label='Chance', alpha=.8)

    mean_tpr = np.mean(tprs_best, axis=0)
    mean_tpr[-1] = 1.0
    
    plt.plot(mean_fpr, mean_tpr, color='b',
             label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
             lw=2, alpha=.8)

    std_tpr = np.std(tprs_best, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ std. dev.')
    plt.xlim([-0.01, 1.01])
    plt.ylim([-0.01, 1.01])
    plt.xlabel('False Positive Rate',fontsize=18)
    plt.ylabel('True Positive Rate',fontsize=18)
    plt.title('Cross-Validation ROC',fontsize=18)
    plt.legend(loc="lower right", prop={'size': 15})
    plt.show()
    fig.savefig(savingdir+'/bestpipe_ROC.pdf')
    return None

def barplot_FeatSelectionRates(feat_votes_df,seuil,savingdir):
    feat_win = feat_votes_df[feat_votes_df>=seuil]
    nfeat = feat_win.shape[0]

    fig = plt.figure(figsize=(16,10))
    if seuil == 75:
        fig = plt.figure(figsize=(14,10)) #20,12
        plt.axes([0.5,0.1,0.4,0.8])
    else:
        plt.axes([0.5,0.1,0.4,0.8])
    ax = fig.gca()
    #ax.set_title(str(seuil)+'% most selected features',fontsize= 18)
    ax.barh(np.arange(0,nfeat), feat_win.values,align='center')
    ax.set_yticks(np.arange(0,nfeat))
    ax.set_yticklabels(adapt_featnames_list(feat_win.index))#, rotation=90)

    ax.tick_params(axis = 'both', which = 'major', labelsize = 18)
    ax.invert_yaxis()
    ax.set_xlabel('Selection rate',fontsize=18)
    fig.savefig(savingdir+'/bestpipe_features_selection_rates_'+str(seuil)+'.pdf')
    return None


def boxplot_FeatScores(feat_scores_df,feat_votes_df,seuil,savingdir):
    selected_feat = feat_votes_df[feat_votes_df>=seuil].index
    scores_win = feat_scores_df[selected_feat]
    scores_boxplot = [scores_win[colname] for colname in selected_feat]
    nfeat = len(selected_feat)
    featn = selected_feat

    new_featn = adapt_featnames_list(featn)

    fig = plt.figure(figsize=(12,10))
    if seuil == 75:
        fig = plt.figure(figsize=(12,10)) #20,12
        plt.axes([0.2,0.55,0.7,0.35])
    else:
        plt.axes([0.2,0.55,0.7,0.35])
    ax=fig.gca()
    plt.title('Importance scores for features with more than '+str(seuil)+'% of selection',fontsize=18)
    plt.boxplot(scores_boxplot,showmeans=True,meanprops={"marker":"s"})
    ax.tick_params(axis = 'both', which = 'major', labelsize = 18)
    plt.xticks(range(1,nfeat+1),new_featn,rotation=80,ha='right')
    plt.ylabel('Normalized importance scores',fontsize=18)

    # create offset transform (x=5pt)
    dx, dy = 10, 0
    offset = ScaledTranslation(dx/fig.dpi, dy/fig.dpi, scale_trans=fig.dpi_scale_trans)
    # apply offset transform to all xticklabels
    for label in ax.xaxis.get_majorticklabels():
        label.set_transform(label.get_transform() + offset)
    plt.show()
    fig.savefig(savingdir+'/bestbipe_features_importances_'+str(seuil)+'.pdf')
    return None

def heatmap_FeatCorr(features,feat_votes_df,seuil,saving_dir):
    selected_feats = feat_votes_df[feat_votes_df>=seuil]
    correlations = features[selected_feats.index].corr()

    featn = features[selected_feats.index].columns.values
    new_featn = adapt_featnames_list(featn)

    if seuil == 75:
        fig = plt.figure(figsize=(24,22)) #20,12
        plt.axes([0.25,0.3,0.8,0.65])
    elif seuil == 90:
        fig = plt.figure(figsize=(24,22))
        plt.axes([0.28,0.28,0.75,0.7])
    else:
        fig = plt.figure(figsize=(12,8))
        plt.axes([0.2,0.2,0.8,0.7])
    ax = fig.gca()
    plot = sns.heatmap(correlations,ax=ax,yticklabels=new_featn,xticklabels=new_featn,annot=True,annot_kws={"size":12})
    plot.set_xticklabels(plot.get_xticklabels(), ha='center')
    plot.set_yticklabels(plot.get_yticklabels(), va='center')
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16)

    plt.yticks(rotation=0)
    plt.show()
    fig.savefig(saving_dir+'/features_correlations_'+str(seuil)+'.pdf')
    return None

def barplot_nb_features_by_pipelines(df,row_index,savingdir,bar_labels=""):
    names = []
    for ppl in df.Pipelines.values:
        if ppl not in names:
            names.append(ppl)

    mean_list = []
    std_list = []
    nb_win = []
    for pipe in names:
        data = df['Nfeats'][df['Pipelines'] == pipe]
        mean_list.append(data.mean())
        std_list.append(data.std())
        nb_win.append(len(data.values))
    
    bar_labels = nb_win

    fig = plt.figure(figsize=(16,10)) #figsize=(16,10)
    plt.axes([0.6,0.2,0.4,0.5])
    ax = fig.gca()
    barplot=ax.barh(np.arange(0,len(names)),mean_list,align='center',xerr=std_list)
    ax.set_yticks(np.arange(0,len(names)))
    if len(bar_labels) != 0:
        barplot_autolabel(ax,barplot,bar_labels)
        ax.set_title("Mean nb of selected feat and Nb of wins")

        try:
            pipenames = adapt_nested_pipenames(names)
        except:
            pipenames = names
        ax.set_yticklabels(pipenames)
    fig.savefig(savingdir+'/nb_features_by_pipelines.pdf')
    return None

def cv_plot(db,pat,target,outcome_name,outcome_dict,tprs,savingdir):
    scores_df = pd.read_csv(savingdir+'/classification_scores.csv',index_col=0)
    misclassif_df = pd.read_csv(savingdir+'/misclassif.csv',index_col=0)
    features_importances_df = pd.read_csv(savingdir+'/features_importances.csv',index_col=0)
    
    # Classif scores histograms
    barplot_best(scores_df,0,'Scores',outcome_name,savingdir,"")
    # Misclassified patients
    heatmap_misclassified(pat,target,misclassif_df,outcome_dict,savingdir,pipes=["All"])
    # Mean Roc
    mean_fpr = np.linspace(0,1,100)
    plot_bestpipe_ROC(mean_fpr,tprs,scores_df['AUC'].values[0],savingdir)

    ## Features importance analysis
    # Feature selection rates for most selected features
    barplot_FeatSelectionRates(features_importances_df.iloc[0],75,savingdir)
    barplot_FeatSelectionRates(features_importances_df.iloc[0],50,savingdir)
    # Feature Importance scores for most selected features
    boxplot_FeatScores(features_importances_df.iloc[1:len(features_importances_df.index)],features_importances_df.iloc[0],75,savingdir)
    boxplot_FeatScores(features_importances_df.iloc[1:len(features_importances_df.index)],features_importances_df.iloc[0],90,savingdir)
    # Feature correlations for most selected features
    try:
        heatmap_FeatCorr(db,features_importances_df.iloc[0],75,savingdir)
    except:
        pass
    try:
        heatmap_FeatCorr(db,features_importances_df.iloc[0],90,savingdir)
    except:
        pass
    
    # Histogram of the number of selected features by pipeline, together with number of folds where the pipeline won.
    selected_pipelines_df = pd.read_csv(savingdir+'/selected_pipelines.csv',index_col=0)
    barplot_nb_features_by_pipelines(selected_pipelines_df,0,savingdir,bar_labels="")
    return None


