#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 19:05:55 2023

Code of the Transcan survival analysis.

im_type in ['MRI','PET','MRI_PET','genes','genes+MRI','genes+PET','genes+MRI_PET']

- Survival :
    100-times repeated 5-fold ncv with Halving GridSearch
    In an external fold : Cox model for univariate analysis;
    Pipeline : feature standardization, Cox+LASSO with coef optimized by internal cv (C-index)
    Scoring : Brier score and integrated Brier Score and C-index.

    Boxplot of features importances
    Correlation heatmaps

    Def of signatures
    a) RS_cox (1)): linear combination of features individually significant from mean pv and mean hazard ratios.
    Based on individual assessment of features in cv.
    b) RS_lasso (several) : signatures identified within one fold each
    c) RS_lasso_cox (1) : same as cox but removing features never detected by LASSO procedures.

    Variations of signatures : iterative Spearman correlation-based filtration
        a) keep features with only lower-threshold correlations
        b) keep features having above-threshold correlations with the larger nb of features.
    i.e when multiple correlations above threshold, choose feature the most correlated with the others.

    Survival stratification
    Median-splitting of the cohort
    Log-rank test
    Propensity score matching
        computation of Propensity scores : logistic regression of recurrence risk
        group as function of EOR, Age, sex, adjuvant therapy, (PET status), histological status and grade.
        1-1 matching from KNN (20 neighbors, radius = 20% of std in propensity scores).

@author: cetchega
"""

##### IMPORTS #####
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import ast
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from scipy.stats import spearmanr
import math

from utils_general import prepare_database, adapt_featnames_list#, adapt_featname
from utils_visualize import PCA_images, PCA_kD
from utils_survival import cv_Lasso_Cox
from utils_survival import characterize_univcox_and_lassocox_df, compute_radiomic_scores, get_correlations_of_best_signatures_after_psm
from utils_survival import get_composition_of_best_signatures_after_psm_and_individual_psm, get_cohort_repartition_before_psm_from_signature

import utils_propensity_score_matching

##### PROG #####
## 0) INITIALIZATION
outcome_name = 'survival' # unchanged
im_type = 'MRI'
type_of_analysis = 'learn_and_analyse' # "learn_and_analyse" to perform the learning process and analyse, or 'analyse_only' to analyze results already saved

nb_repetition = 100
nb_fold = 5



## 1) GENERATE CLEAN DATABASE
# Folders
output_dir = os.path.join('results',outcome_name+'_'+im_type)
if os.path.isdir(output_dir) == False:
    os.mkdir(output_dir)
visudir = os.path.join(output_dir,'visualization')
if os.path.isdir(visudir) == False:
    os.mkdir(visudir)
analysisdir = os.path.join(output_dir,'coxnet')
if os.path.isdir(analysisdir) == False:
     os.mkdir(analysisdir)
resultdir = os.path.join(output_dir,'signatures')
if os.path.isdir(resultdir) == False:
    os.mkdir(resultdir)
genecorrdir = os.path.join(output_dir,'correlation_with_genes')
if os.path.isdir(genecorrdir) == False:
    os.mkdir(genecorrdir)

# Database (with age, sex, grade) - target: PFS, RECURRENCE, Censored
pat, db, target, featnames, outcome_dict, genecols = prepare_database(im_type,outcome_name,output_dir)
cofeat_names = []





## 2) VISUALIZE
PCA_images(db,featnames,target['PFS'],pat,outcome_dict,visudir)
PCA_kD(db,featnames,5,scaling='standard',ret = False)
plt.close('all')






## 3) PERFORM SURVIVAL ANALYSIS by NESTED CROSS VALIDATION (Halving Grid Search)
if type_of_analysis == 'learn_and_analyse':
    # Perform the analysis and save the results
    fold_perf_df = cv_Lasso_Cox(db,cofeat_names,pat,target,analysisdir,outer_nb=nb_fold,n_repeat=nb_repetition,halving=True,prefilter_corr = False)
    fold_perf_df.to_csv(os.path.join(analysisdir,'fold_performances.csv'))
else:
    # Open the results
    fold_perf_df = pd.read_csv(os.path.join(analysisdir,'fold_performances.csv'),index_col=0)








## 4) SHOW PLOTS OF FEATURES PERFORMANCES
# Standard Scaling of the database
db.loc[:,featnames] = StandardScaler().fit_transform(db.loc[:,featnames])
db.columns = ['PatID']+adapt_featnames_list(featnames)

##### Get RadiomicScore_Cox and RadiomicScore_LASSO,COX signatures
## Load other results
all_features_perf_df = pd.read_csv(os.path.join(analysisdir,'all_features_mean_scores.csv'),index_col=0)
all_features_perf_df = all_features_perf_df.rename(columns={'Cox_weights - mean':'Lasso_weights - mean','Cox_weights - std':'Lasso_weights - std'})
#if im_type == 'MRI':
    # for df in [all_coxnet_weights_df,all_univcox_pv_df,all_univcox_hr_df]:
    #     df.columns = [('Flair_')*(name.startswith('original'))+name for name in df.columns.values]
#    all_features_perf_df['features'] = np.array([('Flair_')*(name.startswith('original'))+name for name in all_features_perf_df['features'].values])
all_features_perf_df['features'] = adapt_featnames_list(list(all_features_perf_df['features'].values))

# Check signatures and gather them
characterize_univcox_and_lassocox_df(im_type,db,fold_perf_df,all_features_perf_df,nb_fold*nb_repetition,analysisdir)
all_signatures_df = compute_radiomic_scores(im_type,db,fold_perf_df,all_features_perf_df,analysisdir)










## 5) PROPENSITY SCORE MATCHING AND KAPLAN MEIER ANALYSIS
# Store together both signatures and features
psm_data = all_signatures_df.merge(db,how='inner',on='PatID')

# Get dataframe of survival groups
survival_groups_df = pd.DataFrame(columns = psm_data.columns.values)
for feat in survival_groups_df.columns.values:
    if feat == 'PatID':
        survival_groups_df.loc[:,feat] = psm_data['PatID']
    else:
        med = psm_data[feat].median()
        survival_groups_df.loc[:,feat] = 1*(psm_data.loc[:,feat]>med)
survival_groups_df.to_csv(os.path.join(resultdir,'survival_groups.csv'))

# set the figure shape and size of outputs
sns.set(rc={'figure.figsize':(10,8)}, font_scale = 1.3)
if 'PET' in im_type:
    covar = ['EOR','Age','ADJUVANT Rx','Sex','Histology','Grade']
else:
    covar = ['EOR','Age','ADJUVANT Rx','Sex','Histology','Grade','PET']
target['PatID'] = pat # Very important
survival_groups_df = pd.read_csv(os.path.join(resultdir,'survival_groups.csv'),index_col=0)
utils_propensity_score_matching.PropScoreMatching(survival_groups_df,target,im_type,covar,20,resultdir)


## Take best signatures
logrank_perf_df = pd.read_csv(os.path.join(resultdir,'logrank_results.csv'),index_col=0)
best_signatures = list(logrank_perf_df['Signatures'][logrank_perf_df['LogRank_pv_after_psm']<0.10].values)
best_signatures_05 = list(logrank_perf_df['Signatures'][logrank_perf_df['LogRank_pv_after_psm']<0.05].values)
#best_signatures = ['$'+name+'$' for name in best_signatures]
get_correlations_of_best_signatures_after_psm(best_signatures,best_signatures_05,psm_data,im_type,resultdir)
get_composition_of_best_signatures_after_psm_and_individual_psm(best_signatures,fold_perf_df,psm_data,target,im_type,analysisdir,resultdir)


##### GET COVARIATES REPARTITION BEFORE PSM
signature_name = '$RadiomicScore_{LASSO,318}$'
get_cohort_repartition_before_psm_from_signature(signature_name,target,resultdir)











##### 6) SPEARMAN CORRELATION BETWEEN DEG AND RADIOMIC FEATURES (correlation above 0.5 and pv < 0.05)
# Get intersection database for images and genes
corr_pat, corr_db, corr_target, corr_featnames, corr_outcome_dict, corr_genes = prepare_database(im_type+'_genes',outcome_name,genecorrdir)
corr_db[corr_featnames] = StandardScaler().fit_transform(corr_db[corr_featnames])
corr_radio = [feat for feat in corr_featnames if feat not in corr_genes]
corr_radio = adapt_featnames_list(corr_radio)
corr_featnames = corr_radio+corr_genes
corr_db.columns = ['PatID']+corr_featnames

all_correlations = corr_db.loc[:,corr_featnames].corr(method='spearman')
pval = corr_db.loc[:,corr_featnames].corr(method=lambda x, y: spearmanr(x, y)[1]) - np.eye(*all_correlations.shape)
p = pval.applymap(lambda x: ''.join(['*' for t in [0.01,0.05] if x<=t]))
corr_p_test = all_correlations.round(2).astype(str) + p.astype(str)

abs_correlations = all_correlations.abs()
subcorr = all_correlations.loc[corr_genes,corr_radio]
print("Best correlation = "+str(np.amax(subcorr.values)))
   
# Get all genes and radiomic features that have significant Spearman correlations with absolute value larger than 0.5
threshold = 0.50
npat = len(corr_pat)
best_genes = [gene for gene in corr_genes if np.sum(abs_correlations.loc[gene,corr_radio]>threshold)*np.sum(pval.loc[gene,corr_radio]<0.05)>0]
best_feat = [feat for feat in corr_radio if np.sum(abs_correlations.loc[best_genes,feat]>threshold)*np.sum(pval.loc[best_genes,feat]<0.05)>0]

corr_df = pd.DataFrame(columns=['Feature','Gene','Spearman Correlation','p-value','95%-confidence_interval'])
for feat in best_feat:
     for gene in best_genes:
         if abs_correlations.loc[gene,feat]>threshold and pval.loc[gene,feat]<0.05:
             corr_value = all_correlations.loc[feat,gene].round(2)
             stderr = 1.0 / math.sqrt(npat - 3)
             delta = 1.96 * stderr
             lower = math.tanh(math.atanh(corr_value) - delta)
             upper = math.tanh(math.atanh(corr_value) + delta)
             corr_df.loc[len(corr_df)] = [feat,gene,corr_value,pval.loc[gene,feat].round(5),(round(lower,2),round(upper,2))]
corr_df = corr_df.sort_values(by='p-value')
corr_df.to_csv(os.path.join(genecorrdir,im_type+'_genes_correlations_05.csv'))




